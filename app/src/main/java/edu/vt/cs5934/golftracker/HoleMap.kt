package edu.vt.cs5934.golftracker

import android.annotation.SuppressLint
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Place
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.FabPosition
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MarkerComposable
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.Course
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.service.LocationService
import edu.vt.cs5934.golftracker.ui.theme.GolfBlue
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.HoleViewModel
import kotlinx.coroutines.delay

@SuppressLint("UnrememberedMutableState")
@Composable
fun HoleMap(
    navController: NavHostController,
    holeViewModel: HoleViewModel
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }

    val listOfBalls = if (AppState.currentState.value.isRoundInProgress.value) holeViewModel.listOfBalls
        else GolfRound.golfRound.value.listOfGolfHoles[holeViewModel.replayHoleNumber.intValue - 1].listOfBalls
    val listOfHoles = Course.selectedCourse.value.listOfHoles //courseViewModel.selectedCourse.value.listOfHoles
    val currentHoleNumber = holeViewModel.currentHoleNumber

    val locationService = LocationService()
    val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    val markedToggle = remember { mutableStateOf(false) }
    val endHole = mutableStateOf(false)
    val showClubSelection = mutableStateOf(false)

    LaunchedEffect(holeViewModel.isTimeRunning.value) {
        if (holeViewModel.isTimeRunning.value) {
            while (holeViewModel.remainingTime.intValue > 0) {
                delay(1000)
                holeViewModel.remainingTime.intValue--
            }
            holeViewModel.isTimeRunning.value = false
            holeViewModel.remainingTime.intValue = 10
            holeViewModel.sendNotification(context, "Mark Ball", "Don't forget to mark your ball")
        }
    }

    if (!AppState.currentState.value.isCourseLoaded.value) {
        LoadingScreen()
    } else {
        val hole = if (!AppState.currentState.value.isRoundInProgress.value) {
            listOfHoles.filter { it.holeNumber == holeViewModel.replayHoleNumber.intValue }[0]
        } else {
            listOfHoles.filter { it.holeNumber == currentHoleNumber.intValue }[0]
        }

        val teeLocation = LatLng(hole.teeLocation.latitude, hole.teeLocation.longitude)
        val centerOfGreen = LatLng(hole.centerOfGreen.latitude, hole.centerOfGreen.longitude)
        val bounds = LatLngBounds(
            LatLng(hole.southWestBound.latitude, hole.southWestBound.longitude),
            LatLng(hole.northEastBound.latitude, hole.northEastBound.longitude)
        )

        holeViewModel.holePar.intValue = hole.par
        if (listOfBalls.size == 0) {
            holeViewModel.updateDistanceToGreen(teeLocation, centerOfGreen)
        }

        val properties = MapProperties(
            latLngBoundsForCameraTarget = bounds,
            isMyLocationEnabled = true,
            mapType = MapType.SATELLITE
        )

        val zoom = when (hole.par) {
            3 -> 18.5f
            4 -> 18f
            5 -> 17f
            else -> { 15f }
        }

        val cameraPositionState = rememberCameraPositionState {
            position = CameraPosition.fromLatLngZoom(teeLocation, zoom)
        }

        var isMapLoaded by remember { mutableStateOf(false) }
        Scaffold(
            snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
            bottomBar = { BottomNavBar(
                navController,
                "Home",
                "Scorecard" ,
                "Course",
                R.string.home,
                R.string.scorecard,
                R.string.course_map,
                R.drawable.baseline_home_24,
                R.drawable.ic_score_icon,
                R.drawable.baseline_map_24
            ) },
            floatingActionButton = { if (AppState.currentState.value.isRoundInProgress.value) {
                if (!endHole.value)
                    MarkBallButton(holeViewModel) { holeViewModel.markBall(context, locationService.permissions,
                    fusedLocationClient, centerOfGreen, markedToggle, scope, snackbarHostState,
                    showClubSelection) } } },
            floatingActionButtonPosition = FabPosition.Center
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it)
            ) {
                GoogleMap(
                    modifier = Modifier.fillMaxSize(),
                    onMapLoaded = { isMapLoaded = true },
                    properties = properties,
                    cameraPositionState = cameraPositionState,
                ) {
                    MarkerComposable(
                        state = rememberMarkerState(position = teeLocation),
                        title = "Hole ${hole.holeNumber}"
                    ) {
                        Image(
                            painter = painterResource(R.drawable.ic_golf_ball),
                            contentDescription = "golf ball on a tee",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(25.dp)
                        )
                    }
                    MarkerComposable(
                        state = rememberMarkerState(position = centerOfGreen),
                        title = "Green ${hole.holeNumber}"
                    ) {
                        Image(
                            painter = painterResource(R.drawable.ic_golf_flag),
                            contentDescription = "golf ball on a tee",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(35.dp)
                        )
                    }
                    markedToggle.value
                    listOfBalls.forEach { ball ->
                        val ballLocation = ball.location
                        MarkerComposable(
                            state = rememberMarkerState(position = ballLocation),
                            title = "Shot ${ball.shotNumber}",
                            snippet = if (ball.clubUsed == "") "Distance: ${ball.distance}"
                                    else "Distance: ${ball.distance} Club: ${ball.clubUsed}",
                            onInfoWindowClick = { }
                        ) {
                            Image(
                                painter = painterResource(R.drawable.baseline_sports_golf_24),
                                contentDescription = "white golf ball",
                                modifier = Modifier
                                    .clip(CircleShape)
                                    .size(20.dp)
                            )
                        }
                    }
                }
                //club selection
                if (showClubSelection.value) {
                    ClubSelection(holeViewModel, showClubSelection)
                }
                //putts
                if (endHole.value) {
                    PuttsPopUp(holeViewModel, navController)
                }
                else {
                    //information sidebar
                    Column {
                        Column(
                            modifier = if (AppState.currentState.value.isRoundInProgress.value) Modifier
                                .height(380.dp)
                            else {
                                Modifier
                                    .height(150.dp)
                            }
                                .background(Color.White.copy(alpha = .7f))
                                .width(90.dp),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            markedToggle.value
                            if (AppState.currentState.value.isRoundInProgress.value) {
                                Column(
                                    Modifier.padding(0.dp, 10.dp, 0.dp, 10.dp),
                                    horizontalAlignment = Alignment.CenterHorizontally
                                ) {
                                    Text(text = "Shot")
                                    Text(
                                        text = (listOfBalls.size + 1).toString(),
                                        style = MaterialTheme.typography.displaySmall,
                                        color = Color(0xFF2B5620)
                                    )
                                }
                                HorizontalDivider(
                                    thickness = .4.dp,
                                    color = Color(0xFF262522)
                                )
                                Column(
                                    Modifier.padding(0.dp, 10.dp, 0.dp, 10.dp),
                                    horizontalAlignment = Alignment.CenterHorizontally,
                                ) {
                                    Image(
                                        painter = painterResource(R.drawable.ic_golf_course_icon),
                                        contentDescription = "golf ball on a tee",
                                        modifier = Modifier
                                            .clip(CircleShape)
                                            .size(35.dp)
                                    )
                                    Text(text = "Yards")
                                    Text(
                                        text = if (hole.distance != 0) "${holeViewModel.distanceToGreen.intValue}" else "---",
                                        style = MaterialTheme.typography.displaySmall,
                                        color = Color(0xFF2B5620)
                                    )
                                    Text(
                                        text = holeViewModel.getClubRecommendation(),
                                        fontWeight = FontWeight.Bold
                                    )
                                }
                                HorizontalDivider(
                                    thickness = .4.dp,
                                    color = Color(0xFF262522)
                                )
                            }
                            Column(
                                Modifier.padding(0.dp, 10.dp, 0.dp, 10.dp),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Text(text = "Hole")
                                Text(
                                    text = if (hole.holeNumber != 0) "${hole.holeNumber}" else "---",
                                    fontWeight = FontWeight.Bold
                                )
                            }
                            HorizontalDivider(
                                thickness = .4.dp,
                                color = Color(0xFF262522)
                            )
                            Column(
                                Modifier.padding(0.dp, 5.dp, 0.dp, 5.dp),
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {
                                Text(text = "Par")
                                Text(
                                    text = if (hole.par != 0) "${hole.par}" else "---",
                                    fontWeight = FontWeight.Bold
                                )
                            }
                        }
                        Button(
                            modifier = Modifier
                                .height(60.dp)
                                .width(90.dp)
                                .padding(0.dp, 5.dp, 0.dp, 0.dp),
                            shape = RectangleShape,
                            onClick = { if (AppState.currentState.value.isRoundInProgress.value) {
                                endHole.value = true
                            } else {
                                navController.navigate("Scorecard") {
                                    popUpTo("Course") { inclusive = true }
                                }
                            } }
                        ) {
                            Column(horizontalAlignment = Alignment.CenterHorizontally)
                            {
                                Icon(
                                    modifier = Modifier.size(40.dp),
                                    painter = painterResource(R.drawable.baseline_navigate_next_24),
                                    contentDescription = "arrow",
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun LoadingScreen() {
    Text(
        modifier = Modifier.padding(0.dp, 20.dp, 0.dp, 5.dp),
        text = "Loading course data",
        style = MaterialTheme.typography.titleLarge
    )
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MarkBallButton(viewModel: HoleViewModel, actionToTake: () -> Unit) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .background(
                shape = CircleShape,
                color = GolfGreen
            )
            .size(70.dp)
            .combinedClickable(
                onClick = actionToTake,
                onLongClick = {
                    viewModel.isBallUnplayable.value = true
                    actionToTake.invoke()
                }
            )
    ) {
        Column(modifier = Modifier,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(
                imageVector = Icons.Filled.Place,
                contentDescription = "Floating action button.",
                Modifier.size(30.dp),
                tint = Color.White,
            )
            Spacer(modifier = Modifier.height(2.dp))
            Text(
                text = stringResource(R.string.mark),
                color = Color.White
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun PuttsPopUp(holeViewModel: HoleViewModel, navController: NavHostController) {
    val numOfPutts = remember { mutableIntStateOf(0) }
    val isExpanded = remember { mutableStateOf(false) }
    val icon = if(isExpanded.value) painterResource(R.drawable.baseline_arrow_drop_up_24)
    else painterResource(R.drawable.baseline_arrow_drop_down_24)

    Column(modifier = Modifier
        .fillMaxSize()
        .background(Color.White.copy(alpha = .8f)),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(modifier = Modifier
            .padding(0.dp, 20.dp, 0.dp, 20.dp),
            text = stringResource(R.string.num_putts))

        ExposedDropdownMenuBox(
            expanded = isExpanded.value,
            onExpandedChange = { isExpanded.value = !isExpanded.value },
            modifier = Modifier
        ) {
            OutlinedTextField(
                value = numOfPutts.intValue.toString(),
                onValueChange = { numOfPutts.intValue = it.toInt() },
                label = {Text("Putts")},
                modifier = Modifier
                    .menuAnchor(),
                colors = ExposedDropdownMenuDefaults.outlinedTextFieldColors(),
                trailingIcon = {
                    Icon(
                        painter = icon,
                        contentDescription = "arrow",
                        Modifier.clickable { isExpanded.value = !isExpanded.value }
                    )
                }
            )
            ExposedDropdownMenu(
                expanded = isExpanded.value,
                onDismissRequest = { isExpanded.value = false }
            ) {
                for (num in 0..10) {
                    DropdownMenuItem(
                        text = {
                            Text(
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally),
                                text = num.toString(),
                                textAlign = TextAlign.Center
                            )
                        },
                        onClick = {
                            numOfPutts.intValue = num
                            isExpanded.value = false
                        }
                    )
                }
            }
        }
        Button(
            modifier = Modifier
                .padding(0.dp, 20.dp, 0.dp, 10.dp),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(containerColor = GolfGreen),
            onClick = {
                holeViewModel.completeHole(numOfPutts.intValue)
                { navController.navigate("Scorecard") {
                    popUpTo("Course") { inclusive = true }
                } }
            },
        ) {
            Text(modifier = Modifier,
                text = stringResource(R.string.select),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ClubSelection(holeViewModel: HoleViewModel, showClubSelection: MutableState<Boolean>) {

    val selectedClub = remember { mutableStateOf("") }
    val isExpanded = remember { mutableStateOf(false) }
    val icon = if(isExpanded.value) painterResource(R.drawable.baseline_arrow_drop_up_24)
        else painterResource(R.drawable.baseline_arrow_drop_down_24)

    Column(modifier = Modifier
        .padding(65.dp, 500.dp, 0.dp, 0.dp)
        .border(1.dp, Color.Black, RoundedCornerShape(4.dp))
        .background(color = Color.White.copy(alpha = .7f), shape = RoundedCornerShape(4.dp))
        .height(113.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        ExposedDropdownMenuBox(
            expanded = isExpanded.value,
            onExpandedChange = { isExpanded.value = !isExpanded.value },
            modifier = Modifier
        ) {
            OutlinedTextField(
                value = selectedClub.value,
                onValueChange = { selectedClub.value = it },
                label = {Text("Club")},
                modifier = Modifier
                    .menuAnchor(),
                colors = ExposedDropdownMenuDefaults.outlinedTextFieldColors(),
                trailingIcon = {
                    Icon(
                        painter = icon,
                        contentDescription = "arrow",
                        Modifier.clickable { isExpanded.value = !isExpanded.value }
                    )
                }
            )
            ExposedDropdownMenu(
                expanded = isExpanded.value,
                onDismissRequest = { isExpanded.value = false }
            ) {
                holeViewModel.clubDistances.keys.forEach { club ->
                    DropdownMenuItem(
                        text = {
                            Text(
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally),
                                text = club,
                                textAlign = TextAlign.Center
                            )
                        },
                        onClick = {
                            selectedClub.value = club
                            isExpanded.value = false
                        }
                    )
                }
            }
        }
        Button(
            shape = RoundedCornerShape(4.dp),
            colors = if (selectedClub.value == "") ButtonDefaults.buttonColors(containerColor = Color.LightGray)
                else ButtonDefaults.buttonColors(containerColor = GolfBlue),
            onClick = { if (selectedClub.value == "") { showClubSelection.value = false
                holeViewModel.isTimeRunning.value = true }
                else holeViewModel.addClubToBallMark(selectedClub.value,showClubSelection) }
        ) {
            Text(
                text = if (selectedClub.value == "") stringResource(R.string.dismiss)
                    else stringResource(R.string.select),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }
}