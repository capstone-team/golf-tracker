package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.navigation.BottomNavBar

@Composable
fun EventHomeScreen(
    navController: NavHostController
) {
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        bottomBar = {
            BottomNavBar(
                navController,
                "Home",
                "CommunityHub",
                "Events",
                R.string.home,
                R.string.community_hub,
                R.string.events,
                R.drawable.baseline_home_24,
                R.drawable.community_hub,
                R.drawable.events
            )
        },
        containerColor = Color(0xFFF9FFF8),
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .systemBarsPadding()
                .padding(innerPadding)
                .fillMaxSize(),
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            GolfImage(
                imageRes = R.drawable.event_schedule,
                descriptionId = R.string.event_schedule,
                title = "Event Schedule",
                onClick = { navController.navigate("EventSchedule") }
            )
            GolfImage(
                imageRes = R.drawable.tee_time,
                descriptionId = R.string.tee_time,
                title = "Tee Time",
                onClick = { navController.navigate("EventTeeTime") }
            )
            GolfImage(
                imageRes = R.drawable.live_score,
                descriptionId = R.string.live_score,
                title = "Live Score",
                onClick = { navController.navigate("EventLiveScore") }
            )
        }
    }
}

@Composable
fun GolfImage(
    imageRes: Int,
    descriptionId: Int,
    title: String,
    onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp)
            .padding(16.dp)
            .clip(RoundedCornerShape(16.dp))
            .clickable(onClick = onClick)
    ) {
        Image(
            painter = painterResource(id = imageRes),
            contentDescription = stringResource(id = descriptionId),
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(Color.Transparent, Color.Black.copy(alpha = 0.7f)),
                        startY = 0f,
                        endY = 400f
                    )
                )
        )
        Text(
            text = title,
            color = Color.White,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .align(Alignment.BottomStart)
                .padding(16.dp)
        )
    }
}