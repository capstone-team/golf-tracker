package edu.vt.cs5934.golftracker.screens

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.GolfEvent
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.ui.theme.GolfRed
import edu.vt.cs5934.golftracker.viewmodels.EventScheduleViewModel
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.Calendar

@Composable
fun EventFormScreen(navController: NavHostController, vm: EventScheduleViewModel) {

    val event = vm.currentEventPair.second
    val context = LocalContext.current
    val calendar = Calendar.getInstance()
    val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val dateFormatterDisplay = DateTimeFormatter.ofPattern("d-M-yyyy")
    val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
    val timeFormatterDisplay = DateTimeFormatter.ofPattern("h:mm a")
    val currentTime = LocalTime.now()
    var title by remember { mutableStateOf(event.title) }
    var location by remember { mutableStateOf(event.location) }
    var startDate by remember { mutableStateOf(event.startDate) }
    var endDate by remember { mutableStateOf(event.endDate) }
    val roundedTime = roundToNearestQuarter(currentTime)
    var startTime by remember { mutableStateOf(event.startTime) }
    var endTime by remember { mutableStateOf(event.endTime) }
    var notes by remember { mutableStateOf(event.note) }

    fun convertToDisplayDate(date: String): String {
        return LocalDate.parse(date, dateFormatter).format(dateFormatterDisplay)
    }

    fun convertToDisplayTime(time: String): String {
        return LocalTime.parse(time, timeFormatter).format(timeFormatterDisplay)
    }

    fun convertToSaveDate(date: String): String {
        return LocalDate.parse(date, dateFormatterDisplay).format(dateFormatter)
    }

    fun convertToSaveTime(time: String): String {
        return LocalTime.parse(time.uppercase(), timeFormatterDisplay).format(timeFormatter)
    }

    val startDatePicker = DatePickerDialog(
        context,
        { _, year, month, dayOfMonth ->
            val date = LocalDate.of(year, month + 1, dayOfMonth)
            startDate = date.format(dateFormatter)
            endDate = startDate
        },
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    )

    val startTimePicker = TimePickerDialog(
        context,
        { _, hourOfDay, minute ->
            startTime = LocalTime.of(hourOfDay, minute).format(timeFormatter)
            endTime = startTime
        },
        roundedTime.hour,
        roundedTime.minute,
        false
    )

    val endDatePicker = DatePickerDialog(
        context,
        { _, year, month, dayOfMonth ->
            val date = LocalDate.of(year, month + 1, dayOfMonth)
            endDate = date.format(dateFormatter)
        },
        calendar.get(Calendar.YEAR),
        calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    )

    val endTimePicker = TimePickerDialog(
        context,
        { _, hourOfDay, minute ->
            endTime = LocalTime.of(hourOfDay, minute).format(timeFormatter)
        },
        roundedTime.plusHours(1).hour,
        roundedTime.plusHours(1).minute,
        false
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .systemBarsPadding()
    ) {
        EventHeader(title)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            Spacer(modifier = Modifier.height(24.dp))

            // Title and Location
            OutlinedTextField(
                value = title,
                onValueChange = { title = it },
                label = { Text(stringResource(R.string.title)) },
                modifier = Modifier.fillMaxWidth(),
                shape = RoundedCornerShape(8.dp)
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
                value = location,
                onValueChange = { location = it },
                label = { Text(stringResource(R.string.location)) },
                modifier = Modifier.fillMaxWidth(),
                shape = RoundedCornerShape(8.dp)
            )
            Spacer(modifier = Modifier.height(24.dp))

            // Date and Time
            Text(
                text = "Event Period",
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier.align(Alignment.Start)
            )
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                OutlinedTextField(
                    value = convertToDisplayDate(startDate),
                    onValueChange = { startDate = convertToSaveDate(it) },
                    label = { Text(stringResource(id = R.string.start_date)) },
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = { startDatePicker.show() }) {
                            Icon(
                                Icons.Default.DateRange,
                                contentDescription = stringResource(id = R.string.select_date)
                            )
                        }
                    },
                    modifier = Modifier.weight(1f),
                    shape = RoundedCornerShape(8.dp)
                )
                Spacer(modifier = Modifier.width(8.dp))
                OutlinedTextField(
                    value = convertToDisplayTime(startTime),
                    onValueChange = { startTime = convertToSaveTime(it) },
                    label = { Text(stringResource(id = R.string.time)) },
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = { startTimePicker.show() }) {
                            Icon(
                                Icons.Default.Edit,
                                contentDescription = stringResource(id = R.string.select_time)
                            )
                        }
                    },
                    modifier = Modifier.weight(1f),
                    shape = RoundedCornerShape(8.dp)
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                OutlinedTextField(
                    value = convertToDisplayDate(endDate),
                    onValueChange = { endDate = convertToSaveDate(it) },
                    label = { Text(stringResource(id = R.string.end_date)) },
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = { endDatePicker.show() }) {
                            Icon(
                                Icons.Default.DateRange,
                                contentDescription = stringResource(id = R.string.select_date)
                            )
                        }
                    },
                    modifier = Modifier.weight(1f),
                    shape = RoundedCornerShape(8.dp)
                )
                Spacer(modifier = Modifier.width(8.dp))
                OutlinedTextField(
                    value = convertToDisplayTime(endTime),
                    onValueChange = { endTime = convertToSaveTime(it) },
                    label = { Text(stringResource(id = R.string.time)) },
                    readOnly = true,
                    trailingIcon = {
                        IconButton(onClick = { endTimePicker.show() }) {
                            Icon(
                                Icons.Default.Edit,
                                contentDescription = stringResource(id = R.string.select_time)
                            )
                        }
                    },
                    modifier = Modifier.weight(1f),
                    shape = RoundedCornerShape(8.dp)
                )
            }
            Spacer(modifier = Modifier.height(24.dp))

            // Notes
            OutlinedTextField(
                value = notes,
                onValueChange = { notes = it },
                label = { Text(stringResource(R.string.notes)) },
                modifier = Modifier.fillMaxWidth(),
                shape = RoundedCornerShape(8.dp),
                minLines = 3,
            )
            Spacer(modifier = Modifier.height(32.dp))

            // Action Buttons
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Button(
                    onClick = {
                        val updatedEvent = GolfEvent(
                            typeKey = event.typeKey,
                            title = title,
                            location = location,
                            startDate = startDate,
                            endDate = endDate,
                            startTime = startTime,
                            endTime = endTime,
                            note = notes
                        )
                        vm.addOrUpdateEvent(updatedEvent)
                        navController.navigate("EventSchedule")
                    },
                    shape = RoundedCornerShape(50),
                    colors = ButtonDefaults.buttonColors(containerColor = GolfGreen),
                    modifier = Modifier.size(64.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.save),
                        contentDescription = stringResource(R.string.save),
                        tint = Color.White
                    )
                }
                Button(
                    onClick = {
                        vm.deleteEvent(event.typeKey)
                        navController.navigate("EventSchedule")
                    },
                    shape = RoundedCornerShape(50),
                    colors = ButtonDefaults.buttonColors(containerColor = GolfRed),
                    modifier = Modifier.size(64.dp)
                ) {
                    Icon(
                        painter = painterResource(R.drawable.delete),
                        contentDescription = stringResource(R.string.delete),
                        tint = Color.White
                    )
                }
            }
        }
    }
}

fun roundToNearestQuarter(time: LocalTime): LocalTime {
    val minutes = time.minute
    val roundedMinutes = when {
        minutes < 8 -> 0
        minutes < 23 -> 15
        minutes < 38 -> 30
        minutes < 53 -> 45
        else -> 0
    }
    return time.withMinute(roundedMinutes).withSecond(0).withNano(0)
        .plusHours(if (minutes >= 53) 1 else 0)
}

@Composable
fun EventHeader(title: String) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(GolfGreen, GolfGreen.copy(alpha = 0.8f))
                )
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(R.drawable.events),
                contentDescription = null,
                tint = Color.White,
                modifier = Modifier.size(32.dp)
            )
            Text(
                text = title,
                style = MaterialTheme.typography.titleLarge,
                color = Color.White
            )
            Spacer(modifier = Modifier.size(32.dp))
        }
    }
}