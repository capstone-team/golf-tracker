package edu.vt.cs5934.golftracker.service

import android.Manifest

class LocationService {

    val permissions = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}