package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.viewmodels.SignUpViewModel

@Composable
fun CreateAccountScreen(
    navController: NavHostController,
    viewModel: SignUpViewModel,
    ) {

    val uiState by viewModel.uiState
    val showPasswordError = remember { mutableStateOf(false) }
    val showPasswordLengthError = remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) {innerPadding ->
        Column (
            modifier = Modifier
                .padding(innerPadding)
                .padding(0.dp, 100.dp, 0.dp, 0.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            ShotCaddieLogo()
            Spacer(modifier = Modifier.height(80.dp))
            Text(
                text = stringResource(R.string.password_instruction),
            )
            Spacer(modifier = Modifier.height(16.dp))
            PasswordField(uiState.password, viewModel::onPasswordChange)
            if(showPasswordError.value) {
                PasswordEntryError()
            }
            if(showPasswordLengthError.value) {
                PasswordLengthError()
            }
            Spacer(modifier = Modifier.height(4.dp))
            PasswordField(uiState.confirmPassword, viewModel::onConfirmPasswordChange)
            Spacer(modifier = Modifier.height(16.dp))
            Button(
                modifier = Modifier
                    .width(330.dp)
                    .height(48.dp),
                shape = RoundedCornerShape(4.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Color.Black),
                onClick = {
                    if (uiState.password != uiState.confirmPassword) {
                        showPasswordError.value = true
                    } else if (uiState.password.length < 9) {
                        showPasswordLengthError.value = true
                    } else {
                        viewModel.onSignUp(uiState.email, uiState.password, scope, snackbarHostState) {
                            navController.navigate("Profile") {
                                popUpTo("SignIn") { inclusive = true }
                            }
                        }
                    }
                },
            ) {
                Text(text = stringResource(R.string.create_account),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
        }
    }
}

@Composable
fun PasswordEntryError() {
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = stringResource(R.string.password_error),
        style = MaterialTheme.typography.titleSmall,
        color = Color.Red
    )
}

@Composable
fun PasswordLengthError() {
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = stringResource(R.string.password_length_error),
        style = MaterialTheme.typography.titleSmall,
        color = Color.Red
    )
}