package edu.vt.cs5934.golftracker.data

data class TournamentsResponse(
    val year: String,
    val schedule: List<Tournament>
)

data class Tournament(
    val tournId: String,
    val name: String,
    val date: DateRange,
    val format: String,
    val purse: Any?,
    val fedexCupPoints: Any?
)

data class DateRange(
    val weekNumber: Any?,
    val start: Any?,
    val end: Any?
)

