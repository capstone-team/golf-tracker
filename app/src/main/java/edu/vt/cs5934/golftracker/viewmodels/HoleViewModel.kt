package edu.vt.cs5934.golftracker.viewmodels

import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.ktx.utils.sphericalDistance
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.Ball
import edu.vt.cs5934.golftracker.data.GolfHole
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.utils.metersToYards
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class HoleViewModel : MainAppViewModel() {

    val listOfBalls = mutableListOf<Ball>()
    val currentHoleNumber = mutableIntStateOf(GolfRound.golfRound.value.listOfGolfHoles.size + 1)
    val isHoleInProgress = mutableStateOf(false)
    val distanceToGreen = mutableIntStateOf(0)
    val holePar = mutableIntStateOf(0)
    val isBallUnplayable = mutableStateOf(false)
    val replayHoleNumber = mutableIntStateOf(1)

    private val defaultClubDistances = mapOf("Driver" to 220, "3 Wood" to 205, "5 Wood" to 190, "3 Iron" to 180,
        "4 Iron" to 170, "5 Iron" to 160, "6 Iron" to 150, "7 Iron" to 140, "8 Iron" to 130,
        "9 Iron" to 120, "PW" to 110, "SW" to 95)

    val clubDistances: MutableMap<String, Int> = mutableMapOf<String, Int>().apply {
        defaultClubDistances.forEach { (s, d) ->
            if (User.currentUser.value.listOfClubs[s] != null) {
                val pair = User.currentUser.value.listOfClubs[s]
                this[s] = pair!!.first/pair.second
            }
            else {
                this[s] = d
            }
        }
    }

    private val distanceHit: MutableIntState = mutableIntStateOf(0)

    private val permissionsGranted = mutableStateOf(false)
    private var currentLocation by mutableStateOf<LatLng?>(null)

    val remainingTime = mutableIntStateOf(10)
    val isTimeRunning = mutableStateOf(false)

    fun updateDistanceToGreen(teeLocation: LatLng, centerOfGreen: LatLng) {
        val holeDistance = metersToYards(teeLocation.sphericalDistance(centerOfGreen))
        distanceToGreen.intValue = holeDistance
    }

    fun getClubRecommendation(): String {
        var clubToReturn = ""
        var closestCluDistance = 0

        if (listOfBalls.size == 0 && distanceToGreen.intValue >= clubDistances["Driver"]!!) {
            return "Driver"
        }
        else {
            for (entry in clubDistances.entries) {
                if (entry.value < distanceToGreen.intValue && entry.value > closestCluDistance) {
                    closestCluDistance = entry.value
                    clubToReturn = entry.key
                }
            }
            if (clubToReturn == "Driver") {
                clubToReturn = "3 Wood"
            }
            if (clubToReturn == "") {
                clubToReturn = "PW"
            }
        }
        return clubToReturn
    }

    fun markBall(
        context: Context,
        permissions: Array<String>,
        fusedLocationClient: FusedLocationProviderClient,
        centerOfGreen: LatLng,
        markedToggle: MutableState<Boolean>,
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
        showClubSelection: MutableState<Boolean>
    ) {
        if (permissions.all {
                ContextCompat.checkSelfPermission(
                    context, it
                ) == PackageManager.PERMISSION_GRANTED
            }) {
            permissionsGranted.value = true
            fusedLocationClient.lastLocation.addOnSuccessListener {
                it?.let {
                    val currentLat = it.latitude
                    val currentLon = it.longitude
                    currentLocation = LatLng(currentLat, currentLon)
                }
                val distanceToGreenInMeters = currentLocation?.sphericalDistance(centerOfGreen)
                val oldDistanceToGreen = distanceToGreen.intValue
                distanceToGreen.intValue = metersToYards(distanceToGreenInMeters!!)
                distanceHit.intValue = oldDistanceToGreen - distanceToGreen.intValue

                if (isBallUnplayable.value) {
                    val unplayableBall = Ball(
                        "",
                        currentDate,
                        oldDistanceToGreen - distanceToGreen.intValue,
                        currentHoleNumber.intValue,
                        currentLocation!!,
                        listOfBalls.size + 1
                    )
                    listOfBalls.add(unplayableBall)
                }

                val ballToAdd = Ball(
                    if (isBallUnplayable.value) "Dropped Ball" else "",
                    currentDate,
                    if (isBallUnplayable.value) 0 else oldDistanceToGreen - distanceToGreen.intValue,
                    currentHoleNumber.intValue,
                    currentLocation!!,
                    listOfBalls.size + 1
                )
                listOfBalls.add(ballToAdd)
                markedToggle.value = !markedToggle.value

                scope.launch {
                    val result = snackbarHostState
                        .showSnackbar(
                            "Choose club used?",
                            "Yes",
                            duration = SnackbarDuration.Long
                        )
                    when (result) {
                        SnackbarResult.Dismissed -> { showClubSelection.value = false
                            isTimeRunning.value = true }
                        SnackbarResult.ActionPerformed -> showClubSelection.value = true
                    }
                }
            }
        }
        else {
            scope.launch {
                snackbarHostState.showSnackbar("Failed to determine location.\n" +
                        "Enable location permissions in system privacy settings and try again.")
            }
        }
    }

    fun addClubToBallMark(club: String, showClubSelection: MutableState<Boolean>) {
        if (listOfBalls.isNotEmpty()) {
            if (isBallUnplayable.value) {
                listOfBalls[listOfBalls.lastIndex - 1].clubUsed = club
                isBallUnplayable.value = false
            }
            else {
                listOfBalls[listOfBalls.lastIndex].clubUsed = club
            }

            //update clubs used
            updateClubUsed(club)

            showClubSelection.value = false

            //start notification timer
            isTimeRunning.value = true
        }
    }

    private fun updateClubUsed(club: String) {
        val newPair: Pair<Int, Int>
        if (GolfRound.golfRound.value.clubsUsed[club] != null) {
            val pair = GolfRound.golfRound.value.clubsUsed[club]
            val totalDistance = pair!!.first + distanceHit.intValue
            val count = pair.second + 1
            newPair = Pair(totalDistance, count)
        }
        else {
            newPair = Pair(distanceHit.intValue, 1)
        }
        GolfRound.golfRound.value.clubsUsed[club] = newPair
    }

    fun completeHole(numOfPutts: Int, navigateTo: () -> Unit) {
        val listOfBallsToAdd = mutableListOf<Ball>().apply {
            listOfBalls.forEach { this.add(it) }
        }
        val holeScore = (listOfBalls.size + numOfPutts)
        val golfHole = GolfHole(currentHoleNumber.intValue, holeScore, listOfBallsToAdd, numOfPutts, holePar.intValue)
        GolfRound.golfRound.value.listOfHoleScores.add(golfHole.holeScore)
        GolfRound.golfRound.value.listOfGolfHoles.add(golfHole)
        listOfBalls.clear()
        isHoleInProgress.value = false
        navigateTo()
    }

    fun sendNotification(context: Context, title: String, text: String) {
        val notificationBuilder = NotificationCompat.Builder(context, "markBall")
            .setContentTitle(title)
            .setContentText(text)
            .setSmallIcon(R.drawable.baseline_golf_course_24)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setVibrate(longArrayOf(0, 1000))

        val notificationManager = ContextCompat.getSystemService(
            context,
            NotificationManager::class.java
        ) as NotificationManager

        notificationManager.notify(1, notificationBuilder.build())
    }

    fun incrementReplayHoleNumber() {
        replayHoleNumber.intValue++
    }

    fun decrementReplayHoleNumber() {
        replayHoleNumber.intValue--
    }
}