package edu.vt.cs5934.golftracker.utils

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter

class PairAdapter : TypeAdapter<Pair<Int, Int>>() {
    override fun write(out: JsonWriter, value: Pair<Int, Int>?) {
        out.beginArray()
        out.value(value?.first)
        out.value(value?.second)
        out.endArray()
    }

    override fun read(`in`: JsonReader): Pair<Int, Int> {
        `in`.beginArray()
        val first = `in`.nextInt()
        val second = `in`.nextInt()
        `in`.endArray()
        return Pair(first, second)
    }
}