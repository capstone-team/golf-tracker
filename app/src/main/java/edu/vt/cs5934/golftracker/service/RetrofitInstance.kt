package edu.vt.cs5934.golftracker.service

import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import edu.vt.cs5934.golftracker.utils.DateAdapter
import edu.vt.cs5934.golftracker.utils.TournamentAdapter
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {
    private val moshi = Moshi.Builder()
        .add(DateAdapter())
        .add(TournamentAdapter())
        .add(KotlinJsonAdapterFactory())
        .build()

    val slashApi: SlashGolfApiService by lazy {
        Retrofit.Builder()
            .baseUrl("https://live-golf-data.p.rapidapi.com/")
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(SlashGolfApiService::class.java)
    }
}