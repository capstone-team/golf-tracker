package edu.vt.cs5934.golftracker.viewmodels

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.Firebase
import com.google.firebase.firestore.firestore
import edu.vt.cs5934.golftracker.data.Tournament
import edu.vt.cs5934.golftracker.data.GolfEvent
import edu.vt.cs5934.golftracker.data.GolfEvents
import edu.vt.cs5934.golftracker.data.User
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import kotlin.random.Random

private const val TOUR_YEAR: String = "2024"
private const val ID: String = "1"

class EventScheduleViewModel : ViewModel() {
    lateinit var currentEventPair: Pair<String, GolfEvent>

    private val firestore = Firebase.firestore
    private val eventsCollection = firestore.collection("events")

    private val _events = MutableStateFlow<List<GolfEvents>>(emptyList())
    private val events: StateFlow<List<GolfEvents>> = _events.asStateFlow()

    init {
        fetchEvents()
    }

    private val _filterOption = MutableStateFlow(FilterOption.ALL)
    val filterOption: StateFlow<FilterOption> = _filterOption.asStateFlow()

    private val _locationFilter = MutableStateFlow("")
    val locationFilter: StateFlow<String> = _locationFilter.asStateFlow()

    private val _filteredEvents = MutableStateFlow<List<GolfEvents>>(emptyList())
    val filteredEvents: StateFlow<List<GolfEvents>> = _filteredEvents.asStateFlow()

    enum class FilterOption {
        ALL, NATIONAL_TOURNAMENTS, MY_EVENTS, LOCATION
    }

    fun setFilterOption(option: FilterOption) {
        _filterOption.value = option
        applyFilter()
    }

    fun setLocationFilter(location: String) {
        _locationFilter.value = location.uppercase()
        applyFilter()
    }


    private fun filterEventsByTypeKey(events: List<GolfEvents>): List<GolfEvents> {
        return events.map { golfEvents ->
            GolfEvents(
                golfEvents.date,
                golfEvents.events.filter { (typeKey, _) ->
                    !typeKey.startsWith("E") || typeKey.startsWith("E${User.currentUser.value.guid}")
                }.toMutableMap()
            )
        }.filter { it.events.isNotEmpty() }
    }

    private fun applyFilter() {
        viewModelScope.launch {
            val allEvents = filterEventsByTypeKey(_events.value)
            val currentDate = LocalDate.now()

            _filteredEvents.value = when (_filterOption.value) {
                FilterOption.ALL -> filterFutureEvents(allEvents, currentDate)
                FilterOption.NATIONAL_TOURNAMENTS -> filterFutureEvents(
                    allEvents.map { golfEvents ->
                        GolfEvents(
                            golfEvents.date,
                            golfEvents.events.filter { (typeKey, _) -> typeKey.first().isDigit() }.toMutableMap()
                        )
                    }.filter { it.events.isNotEmpty() },
                    currentDate
                )
                FilterOption.MY_EVENTS -> filterFutureEvents(
                    allEvents.map { golfEvents ->
                        GolfEvents(
                            golfEvents.date,
                            golfEvents.events.filter { (typeKey, _) -> typeKey.startsWith("E") }.toMutableMap()
                        )
                    }.filter { it.events.isNotEmpty() },
                    currentDate
                )
                FilterOption.LOCATION -> filterFutureEvents(
                    allEvents.map { golfEvents ->
                        GolfEvents(
                            golfEvents.date,
                            golfEvents.events.filter { (_, event) ->
                                event.location.uppercase().endsWith(_locationFilter.value)
                            }.toMutableMap()
                        )
                    }.filter { it.events.isNotEmpty() },
                    currentDate
                )
            }.sortedWith(compareBy<GolfEvents> { it.date }
                .thenBy { golfEvents ->
                    golfEvents.events.values.map { it.location }.minOf { location ->
                        location.takeLast(2) + location.dropLast(2)
                    }
                })
        }
    }

    private fun filterFutureEvents(events: List<GolfEvents>, currentDate: LocalDate): List<GolfEvents> {
        return events.mapNotNull { golfEvents ->
            val eventDate = LocalDate.parse(golfEvents.date)
            if (eventDate >= currentDate) {
                golfEvents
            } else {
                null
            }
        }
    }

    // Update this function to apply the filter whenever events are updated
    private fun updateEventsState(eventsList: List<GolfEvent>) {
        val groupedEvents = eventsList.groupBy { it.startDate }
        _events.value = groupedEvents.map { (date, events) ->
            GolfEvents(date, events.associateBy { it.typeKey }.toMutableMap())
        }.sortedBy { it.date }
        applyFilter()
    }

    fun fetchTournaments() {
        viewModelScope.launch {
            try {
                /* ONLY USE FOR DEMONSTRATION DUE TO API Call limit
                val response = RetrofitInstance.slashApi.getSchedule(ID, TOUR_YEAR)
                Log.d(TAG, "Fetched ${response.schedule.size} tournaments")
                response.schedule.forEach { tour ->
                    val event = createEventFromTournament(tour)
                    addOrUpdateEvent(event)
                }
                 */
            } catch (e: Exception) {
                Log.e(TAG, "Error fetching tournaments", e)
            }
        }
    }

    private fun createEventFromTournament(tour: Tournament): GolfEvent {
        // format date, and purse
        val purse = tour.purse.toString().substring(
            12, tour.purse.toString().length - 1
        )
        val start = convertToDateStr(
            tour.date.start.toString().substring(
                20, tour.date.start.toString().length - 2
            )
        )
        val end = convertToDateStr(
            tour.date.end.toString().substring(
                20, tour.date.end.toString().length - 2
            )
        )

        return GolfEvent(
            typeKey = tour.tournId,
            title = tour.name,
            startDate = start.toLocalDate().toString(),
            endDate = end.toLocalDate().toString(),
            startTime = start.toLocalTime().toString(),
            endTime = end.toLocalTime().toString(),
            note = "Tournament ID: ${tour.tournId}\n" +
                    "Format: ${tour.format}\n" +
                    "Purse: $purse\n" +
                    "Fedex Cup Points: ${tour.fedexCupPoints ?: 0}"
        )
    }

    private fun fetchEvents() {
        viewModelScope.launch {
            eventsCollection.addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.e(TAG, "Error fetching events", error)
                    return@addSnapshotListener
                }

                if (snapshot != null) {
                    val eventsList = snapshot.documents.mapNotNull { doc ->
                        doc.toObject(GolfEvent::class.java)
                    }
                    updateEventsState(eventsList)
                }
            }
        }
    }

    fun addOrUpdateEvent(event: GolfEvent) {
        viewModelScope.launch {
            try {
                eventsCollection.document(event.typeKey).set(event)
                fetchEvents() // Fetch all events again
                applyFilter() // Re-apply the current filter
                Log.d(TAG, "Event added/updated successfully")
            } catch (e: Exception) {
                Log.e(TAG, "Error adding/updating event", e)
            }
        }
    }

    fun deleteEvent(eventTypeKey: String) {
        viewModelScope.launch {
            try {
                eventsCollection.document(eventTypeKey).delete()
                Log.d(TAG, "Event deleted successfully")
            } catch (e: Exception) {
                Log.e(TAG, "Error deleting event", e)
            }
        }
    }

    private fun convertToDateStr(value: String): LocalDateTime {
        val instant = Instant.ofEpochMilli(value.toLong())
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
    }

    fun generateNewManualEventTypeKey(): String {
        val timestamp = System.currentTimeMillis()
        val random = Random.nextInt(1000, 9999)
        return "E${User.currentUser.value.guid}-$timestamp-$random"
    }
}