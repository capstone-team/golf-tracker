package edu.vt.cs5934.golftracker.data

data class SignUpUIState(
    val email: String = "",
    val password: String = "",
    val confirmPassword: String = ""
)

