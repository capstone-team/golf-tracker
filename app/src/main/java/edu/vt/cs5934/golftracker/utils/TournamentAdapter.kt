package edu.vt.cs5934.golftracker.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import edu.vt.cs5934.golftracker.data.DateRange
import edu.vt.cs5934.golftracker.data.Tournament

class TournamentAdapter {
    @FromJson
    fun fromJson(json: Map<String, Any?>): Tournament {
        return Tournament(
            tournId = json["tournId"] as? String ?: "",
            name = json["name"] as? String ?: "",
            date = (json["date"] as? Map<*, *>)?.let { DateAdapter().fromJson(it) } ?: DateRange(
                "",
                "",
                ""
            ),
            format = json["format"] as? String ?: "",
            purse = json["purse"] ?: "",
            fedexCupPoints = json["fedexCupPoints"] ?: ""
        )
    }

    @ToJson
    fun toJson(tournament: Tournament): Map<String, Any?> {
        return mapOf(
            "tournId" to tournament.tournId,
            "name" to tournament.name,
            "date" to DateAdapter().toJson(tournament.date),
            "format" to tournament.format,
            "purse" to tournament.purse,
            "fedexCupPoints" to tournament.fedexCupPoints
        )
    }
}