package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfBlue
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ClubScreen(
    navController: NavHostController
) {

    val clubData = User.currentUser.value.listOfClubs

    val driver = remember { clubData.filter { (club) -> club == "Driver" } }
    val fairwayWoods = remember { clubData.filter { (club) -> club.contains("Wood") } }
    val longIrons = remember { clubData.filter { (club) -> club == "3 Iron" || club == "4 Iron" } }
    val midIrons = remember { clubData.filter { (club) -> club == "5 Iron" || club == "6 Iron" || club == "7 Iron"} }
    val shortIrons = remember { clubData.filter { (club) -> club == "8 Iron" || club == "9 Iron" } }
    val pitching = remember { clubData.filter { (club) -> club == "PW" || club == "SW" } }

    val popUpClubs = remember { mutableStateOf<Map<String, Pair<Int, Int>>>(mapOf()) }
    val showPopUp = remember { mutableStateOf(false) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(stringResource(R.string.club_stats)) },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = GolfGreen,
                    titleContentColor = Color.White
                )
            )
        },
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "Community Hub" ,
            "Events",
            R.string.home,
            R.string.community_hub,
            R.string.events,
            R.drawable.baseline_home_24,
            R.drawable.community_hub,
            R.drawable.events
        ) },
        containerColor = Color(0xFFF9FFF8),
    ) {innerPadding ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding)
        ) {
            Column(
                modifier = Modifier
                    .padding(0.dp, 50.dp, 0.dp, 0.dp),
                verticalArrangement = Arrangement.SpaceEvenly
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    ClubType(
                        imageRes = R.drawable.driver,
                        descriptionId = R.string.driver,
                        title = "Driver",
                        onClick = {
                            popUpClubs.value = driver
                            showPopUp.value = true
                        }
                    )
                    ClubType(
                        imageRes = R.drawable.fairway_woods,
                        descriptionId = R.string.fairway_wood,
                        title = "Fairway Woods",
                        onClick = {
                            popUpClubs.value = fairwayWoods
                            showPopUp.value = true
                        }
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    ClubType(
                        imageRes = R.drawable.long_iron,
                        descriptionId = R.string.long_iron,
                        title = "Long Irons",
                        onClick = {
                            popUpClubs.value = longIrons
                            showPopUp.value = true
                        }
                    )
                    ClubType(
                        imageRes = R.drawable.mid_iron,
                        descriptionId = R.string.mid_iron,
                        title = "Mid Irons",
                        onClick = {
                            popUpClubs.value = midIrons
                            showPopUp.value = true
                        }
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    ClubType(
                        imageRes = R.drawable.short_iron,
                        descriptionId = R.string.short_iron,
                        title = "Short Irons",
                        onClick = {
                            popUpClubs.value = shortIrons
                            showPopUp.value = true
                        }
                    )
                    ClubType(
                        imageRes = R.drawable.pitching,
                        descriptionId = R.string.pitching,
                        title = "Pitching & Chipping",
                        onClick = {
                            popUpClubs.value = pitching
                            showPopUp.value = true
                        }
                    )
                }
            }
            if (showPopUp.value) {
                ClubPopup(
                    clubs = popUpClubs.value,
                    onClick = {
                        popUpClubs.value = mapOf()
                        showPopUp.value = false
                    }
                )
            }
        }
    }
}

@Composable
fun ClubType(
    imageRes: Int,
    descriptionId: Int,
    title: String,
    onClick: () -> Unit
) {
    Box(
        modifier = Modifier
            .height(200.dp)
            .width(200.dp)
            .padding(10.dp, 10.dp, 0.dp, 0.dp)
            .clip(RoundedCornerShape(4.dp))
            .clickable(onClick = onClick),
    ) {
        Image(
            painter = painterResource(id = imageRes),
            contentDescription = stringResource(id = descriptionId),
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxSize()
        )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(Color.Transparent, Color.Black.copy(alpha = 0.7f)),
                        startY = 0f,
                        endY = 400f
                    )
                )
        )
        Text(
            text = title,
            color = Color.White,
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .align(Alignment.BottomStart)
                .padding(16.dp)
        )
    }
}

@Composable
fun ClubPopup(clubs: Map<String, Pair<Int, Int>>, onClick: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(50.dp, 200.dp, 50.dp, 0.dp)
            .background(
                brush = Brush.horizontalGradient(
                    colors = listOf(Color(0xFFDFF6D9), Color(0xFFF6F9F5))
                ),
                shape = RoundedCornerShape(24.dp)
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (clubs.isEmpty()) {
            Text(
                text = "No club use to display",
                modifier = Modifier
                    .padding(0.dp, 10.dp, 0.dp, 0.dp),
                style = MaterialTheme.typography.titleMedium,
                fontWeight = FontWeight.Bold,
                fontSize = 18.sp
            )
            Text(
                text = "Get Golfing",
                style = MaterialTheme.typography.titleMedium,
                fontWeight = FontWeight.Medium,
                fontSize = 40.sp,
                color = GolfGreen
            )
        } else {
            clubs.forEach{ club ->
                Text(
                    text = club.key,
                    modifier = Modifier
                        .padding(0.dp, 10.dp, 0.dp, 0.dp),
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    fontSize = 18.sp
                )
                Text(
                    text = "${(club.value.first / club.value.second)} yds",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Medium,
                    fontSize = 40.sp,
                    color = GolfGreen
                )
            }
        }
        Button(
            modifier = Modifier
                .align(Alignment.End)
                .padding(0.dp, 10.dp, 10.dp, 10.dp),
            shape = RoundedCornerShape(24.dp),
            colors = ButtonDefaults.buttonColors(containerColor = GolfBlue),
            onClick = onClick
        ) {
            Text(text = stringResource(R.string.ok))
        }
    }
}