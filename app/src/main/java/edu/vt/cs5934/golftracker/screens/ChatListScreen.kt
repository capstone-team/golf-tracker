package edu.vt.cs5934.golftracker.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.ChatNotification
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfContainer
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.ChatListViewModel
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChatListScreen(
    navController: NavHostController,
    vm: ChatListViewModel
) {
    val users by vm.users.collectAsState()
    val blockedUserIds by vm.blockedUserIds.collectAsState()
    val activeConversationIds by vm.activeConversationIds.collectAsState()
    val listState = rememberLazyListState()
    var isLoading by remember { mutableStateOf(false) }
    val notification by vm.notification.collectAsState()

    LaunchedEffect(listState) {
        snapshotFlow { listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index }
            .collect { lastIndex ->
                if (lastIndex != null && lastIndex >= users.size - 1 && !isLoading) {
                    isLoading = true
                    vm.loadMoreUsers()
                    isLoading = false
                }
            }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("Community Hub", color = Color.White) },
                    colors = TopAppBarDefaults.topAppBarColors(containerColor = GolfGreen)
                )
            },
            bottomBar = {
                BottomNavBar(
                    navController,
                    "Home",
                    "CommunityHub",
                    "Events",
                    R.string.home,
                    R.string.community_hub,
                    R.string.events,
                    R.drawable.baseline_home_24,
                    R.drawable.community_hub,
                    R.drawable.events
                )
            },
            containerColor = GolfContainer,
        ) { paddingValues ->
            Column(modifier = Modifier.padding(paddingValues)) {
                HandicapRangeCard()
                LazyColumn(
                    state = listState,
                    contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
                    verticalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    items(users) { user ->
                        UserCard(
                            navController = navController,
                            user = user,
                            vm = vm,
                            isBlocked = blockedUserIds.contains(user.guid),
                            isActiveConversation = activeConversationIds.contains(user.guid)
                        )
                    }
                    item {
                        if (isLoading) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(16.dp),
                                contentAlignment = Alignment.Center
                            ) {
                                CircularProgressIndicator(color = GolfGreen)
                            }
                        }
                    }
                }
            }
        }

        AnimatedVisibility(
            visible = notification != null,
            enter = slideInVertically() + expandVertically(expandFrom = Alignment.Top) + fadeIn(),
            exit = slideOutVertically() + shrinkVertically(shrinkTowards = Alignment.Top) + fadeOut(),
            modifier = Modifier.align(Alignment.TopCenter)
        ) {
            notification?.let { chatNotification ->
                NotificationBanner(
                    notification = chatNotification,
                    onDismiss = { vm.clearNotification() },
                    onAction = {
                        navController.navigate("Chat/${chatNotification.senderId}/${chatNotification.senderName}")
                        vm.clearNotification()
                    },
                    modifier = Modifier.padding(top = 64.dp)
                )
            }
        }
    }
}

@Composable
fun NotificationBanner(
    notification: ChatNotification,
    onDismiss: () -> Unit,
    onAction: () -> Unit,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
        colors = CardDefaults.cardColors(containerColor = GolfGreen)
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column {
                Text(
                    text = notification.senderName,
                    style = MaterialTheme.typography.titleSmall,
                    color = Color.White
                )
                Text(
                    text = notification.body,
                    style = MaterialTheme.typography.bodyMedium,
                    color = Color.White
                )
            }
            TextButton(onClick = onAction) {
                Text("View", color = Color.White)
            }
        }
    }
}

@Composable
fun HandicapRangeCard() {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White)
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Text(
                "Handicap Range",
                style = MaterialTheme.typography.titleMedium,
                color = GolfGreen
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                "Showing users with handicap between ${User.currentUser.value.handicap - 10} and ${User.currentUser.value.handicap + 10}",
                style = MaterialTheme.typography.bodyMedium,
                color = Color.Gray
            )
        }
    }
}

@Composable
fun UserCard(
    navController: NavHostController,
    user: User,
    vm: ChatListViewModel,
    isBlocked: Boolean,
    isActiveConversation: Boolean
) {
    var showOptions by remember { mutableStateOf(false) }

    Card(
        onClick = {
            if (!isBlocked) {
                navController.navigate("Chat/${user.guid}/${user.name}")
                vm.addActiveConversation(user.guid)
            }
        },
        modifier = Modifier.fillMaxWidth(),
        elevation = CardDefaults.cardElevation(defaultElevation = 2.dp),
        colors = CardDefaults.cardColors(
            containerColor = when {
                isBlocked -> Color.Red.copy(alpha = 0.1f)
                isActiveConversation -> Color.Green.copy(alpha = 0.1f)
                else -> Color.White
            }
        )
    ) {
        Row(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_account_circle_24),
                    contentDescription = null,
                    modifier = Modifier.size(48.dp),
                    tint = if (isBlocked) Color.Red else GolfGreen
                )
                Spacer(modifier = Modifier.width(16.dp))
                Column {
                    Text(
                        text = user.name,
                        style = MaterialTheme.typography.titleMedium,
                        color = if (isBlocked) Color.Red else Color.Black
                    )
                    Spacer(modifier = Modifier.height(4.dp))
                    Text(
                        text = "Handicap: ${user.handicap}",
                        style = MaterialTheme.typography.bodyMedium,
                        color = if (isBlocked) Color.Red.copy(alpha = 0.7f) else Color.Gray
                    )
                }
            }
            IconButton(onClick = { showOptions = true }) {
                Icon(
                    imageVector = Icons.Default.MoreVert,
                    contentDescription = "More options",
                    tint = if (isBlocked) Color.Red else GolfGreen
                )
            }
        }
    }

    if (showOptions) {
        AlertDialog(
            onDismissRequest = { showOptions = false },
            title = { Text("Options") },
            text = { Text("What would you like to do?") },
            confirmButton = {
                TextButton(onClick = {
                    if (isBlocked) {
                        vm.unblockUser(user.guid)
                    } else {
                        vm.blockUser(user.guid)
                    }
                    showOptions = false
                }) {
                    Text(if (isBlocked) "Unblock User" else "Block User")
                }
            },
            dismissButton = {
                TextButton(onClick = { showOptions = false }) {
                    Text("Cancel")
                }
            }
        )
    }
}