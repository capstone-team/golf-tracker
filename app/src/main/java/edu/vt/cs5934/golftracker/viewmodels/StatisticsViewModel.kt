package edu.vt.cs5934.golftracker.viewmodels

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import edu.vt.cs5934.golftracker.data.GolfHole
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.utils.mapToGolfHole
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.Locale

class StatisticsViewModel: MainAppViewModel() {

    val selectedRound = mutableStateOf(GolfRound())
    val courseNames = mutableListOf<String>()
    val datesPlayed = mutableListOf<String>()
    val targets = mutableListOf<Float>()
    val greensInReg = mutableListOf<Float>()

    fun retrieveCourseNames(
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
    ) {
        firestore.collection("courseNames")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result) {
                        val obj = document.data
                        val index = obj.getValue("courseId").toString().toInt()
                        val name = obj.getValue("name").toString()
                        courseNames.add(index - 1, name)
                    }
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to retrieve courses. " +
                                "Restart and try again")
                    }
                }
            }
    }

    fun getRoundDates(
        courseName: String,
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState
    ) {
        firestore.collection("users").document(User.currentUser.value.guid)
            .collection(courseName)
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result) {
                        val obj = document.data
                        val date = obj.getValue("date").toString()
                        datesPlayed.add(date)
                    }
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to retrieve dates. " +
                                "Restart and try again")
                    }
                }
            }
    }

    fun retrieveRoundAndCourse(
        courseName: String,
        datePlayed: String,
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
        navigateTo: () -> Unit
    ) {
        datesPlayed.clear()

        //retrieve course
        CourseViewModel().retrieveAndLoadCourse(courseName, scope, snackbarHostState, false) { }

        //retrieve round
        firestore.collection("users").document(User.currentUser.value.guid)
            .collection(courseName)
            .whereEqualTo("date", datePlayed)
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result) {
                        val obj = document.data
                        val name = obj.getValue("courseName").toString()
                        val date = obj.getValue("date").toString()
                        val listOfHoles = obj.getValue("listOfGolfHoles") as MutableList<HashMap<String, Any>>
                        val listOfHoleScores = obj.getValue("listOfHoleScores") as MutableList<Int>
                        val roundScore = obj.getValue("roundScore").toString().toInt()

                        //convert from HashMap to GolfHoles
                        val listOfGolfHoles: MutableList<GolfHole> = mutableListOf<GolfHole>().apply {
                            listOfHoles.forEach { hole ->
                                val golfHole = mapToGolfHole(hole)
                                this.add(golfHole)
                            }
                        }

                        selectedRound.value = GolfRound(mutableMapOf(), name, date, listOfGolfHoles,
                            listOfHoleScores, roundScore)

                        GolfRound.golfRound.value.courseName = selectedRound.value.courseName
                        GolfRound.golfRound.value.date = selectedRound.value.date
                        selectedRound.value.listOfGolfHoles.forEach {golfHole ->
                            GolfRound.golfRound.value.listOfGolfHoles.add(golfHole)
                        }
                        selectedRound.value.listOfHoleScores.forEach {score ->
                            GolfRound.golfRound.value.listOfHoleScores.add(score)
                        }
                        GolfRound.golfRound.value.roundScore = selectedRound.value.roundScore
                    }
                    getGreensInRegulation(selectedRound.value)
                    populateTargets()
                    navigateTo()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to retrieve course. " +
                                "Restart and try again")
                    }
                }
            }
    }

    private fun getGreensInRegulation(round: GolfRound): Double {
        greensInReg.clear()
        var count = 0
        round.listOfGolfHoles.forEach { hole ->
            if (hole.par - hole.listOfBalls.size >= 2) {
                count++
            }
        }

        val gir = (count / selectedRound.value.listOfGolfHoles.size.toDouble()) * 100
        greensInReg.add(gir.toFloat())
        greensInReg.add((100 - gir).toFloat())

        return String.format(Locale.US, "%.2f",
            (count / round.listOfGolfHoles.size.toDouble())*100).toDouble()
    }

    fun getNumOfThreePutts(round: GolfRound): Int {
        var count = 0
        round.listOfGolfHoles.forEach { hole ->
            if (hole.numOfPutts >= 3) {
                count++
            }
        }
        return count
    }

    fun getAvgDrivingDistance(round: GolfRound): Double {
        var count = 0
        var totalDistance = 0.0
        round.listOfGolfHoles.forEach { hole ->
            val teeShot = hole.listOfBalls[0]
            if (teeShot.clubUsed == "Driver") {
                count++
                totalDistance += teeShot.distance
            }
        }
        return String.format(Locale.US, "%.1f",
            (totalDistance / count)).toDouble()
    }

    fun getLongestDrive(round: GolfRound): Int {
        var longestDrive = 0
        round.listOfGolfHoles.forEach { hole ->
            val teeShot = hole.listOfBalls[0]
            if (teeShot.clubUsed == "Driver") {
                if (teeShot.distance > longestDrive)
                    longestDrive = teeShot.distance
            }
        }
        return longestDrive
    }

    fun getAvgNumOfPutts(round: GolfRound): Double {
        var totalPutts = 0
        round.listOfGolfHoles.forEach { hole ->
            totalPutts += hole.numOfPutts
        }

        return String.format(Locale.US, "%.2f",
            totalPutts / round.listOfGolfHoles.size.toDouble()).toDouble()
    }

    private fun populateTargets() {
        targets.clear()
        var birdies = 0
        var pars = 0
        var bogeys = 0

        selectedRound.value.listOfGolfHoles.forEach {golfHole ->
            if (golfHole.holeScore < golfHole.par) {
                birdies++
            }
            else if (golfHole.par == golfHole.holeScore) {
                pars++
            }
            else {
                bogeys++
            }
        }
        targets.add(birdies.toFloat())
        targets.add(pars.toFloat())
        targets.add(bogeys.toFloat())
    }
}