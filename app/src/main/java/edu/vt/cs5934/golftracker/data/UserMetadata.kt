package edu.vt.cs5934.golftracker.data

data class UserMetadata(
    val userId: String = "",
    val blockedUsers: List<String> = listOf(),
    val activeConversations: List<String> = listOf()
)