package edu.vt.cs5934.golftracker.data

import com.google.firebase.firestore.PropertyName
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

class GolfEvent(
    var typeKey: String = "",
    var title: String = "",
    var location: String = "",
    @get:PropertyName("startDate")
    @set:PropertyName("startDate")
    var startDate: String = LocalDate.now().toString(),
    @get:PropertyName("endDate")
    @set:PropertyName("endDate")
    var endDate: String = LocalDate.now().toString(),
    @get:PropertyName("startTime")
    @set:PropertyName("startTime")
    var startTime: String = LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME),
    @get:PropertyName("endTime")
    @set:PropertyName("endTime")
    var endTime: String = LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME),
    var note: String = ""
) {
    fun getStartDateAsLocalDate(): LocalDate = LocalDate.parse(startDate)
    fun getEndDateAsLocalDate(): LocalDate = LocalDate.parse(endDate)
    fun getStartTimeAsLocalTime(): LocalTime = LocalTime.parse(startTime)
    fun getEndTimeAsLocalTime(): LocalTime = LocalTime.parse(endTime)

    fun setStartDateFromLocalDate(date: LocalDate) {
        startDate = date.toString()
    }

    fun setEndDateFromLocalDate(date: LocalDate) {
        endDate = date.toString()
    }

    fun setStartTimeFromLocalTime(time: LocalTime) {
        startTime = time.format(DateTimeFormatter.ISO_LOCAL_TIME)
    }

    fun setEndTimeFromLocalTime(time: LocalTime) {
        endTime = time.format(DateTimeFormatter.ISO_LOCAL_TIME)
    }
}