package edu.vt.cs5934.golftracker.viewmodels

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.ViewModel
import com.google.firebase.Firebase
import com.google.firebase.firestore.firestore
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.service.AccountServiceImpl
import edu.vt.cs5934.golftracker.utils.docToUser
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

open class MainAppViewModel : ViewModel() {

    val firestore = Firebase.firestore
    val accountService = AccountServiceImpl()

    private val dateFormatter: DateFormat = SimpleDateFormat.getDateInstance()
    val currentDate: String = dateFormatter.format(Date())

    fun updateUser() {
        if (accountService.hasUser()) {
            val currentUserId = accountService.currentUserId
            try {
                firestore.collection("users").document(currentUserId)
                    .get()
                    .addOnSuccessListener { document ->
                        User.currentUser.value = docToUser(document)
                    }
            } catch (e: Exception) {
                Log.e(TAG, "UpdateUserError: ${e.message}")
            }
        }
    }

    fun getGolfTips() {
        try {
            firestore.collection("tips").document("golf_tips")
                .get()
                .addOnSuccessListener { document ->
                    val obj = document.data
                    val tips = obj?.getValue("tip") as MutableList<String>

                    tips.forEach { tip ->
                        AppState.currentState.value.golfTips.add(tip)
                    }
                }
        } catch (e: Exception) {
            Log.e(TAG, "GolfTip Error: ${e.message}")
        }
    }
}