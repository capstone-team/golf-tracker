package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.service.LocationService
import edu.vt.cs5934.golftracker.ui.theme.GolfBlue
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.CourseViewModel
import edu.vt.cs5934.golftracker.viewmodels.HoleViewModel
import edu.vt.cs5934.golftracker.viewmodels.StatisticsViewModel
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun HomeScreen(
    navController: NavHostController,
    holeViewModel: HoleViewModel,
    courseViewModel: CourseViewModel,
    statisticsViewModel: StatisticsViewModel,
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val locationService = LocationService()
    val permissionsGranted = remember { mutableStateOf(false) }

    val requestPermissionsLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissionsMap ->
        val areGranted = permissionsMap.values.reduce { acc, next -> acc && next }
        if (areGranted) {
            permissionsGranted.value = true
        }
        else {
            scope.launch {
                permissionsGranted.value = false
                snackbarHostState.showSnackbar("Shot Caddie requires access to precise location services." +
                        " Change permissions in system privacy settings and try again.")
            }
        }
    }

    fun requestPermissions(permissions: Array<String>) {
        if (permissions.all {
                ContextCompat.checkSelfPermission(
                    context, it
                ) == PackageManager.PERMISSION_GRANTED
            }) {
            permissionsGranted.value = true

            if (holeViewModel.isHoleInProgress.value) {
                navController.navigate("Hole")
            }
            else if (AppState.currentState.value.isRoundInProgress.value) {
                navController.navigate("Course")
            }
            else if (courseViewModel.courseNames.size < 1) {
                courseViewModel.retrieveCourseNames(scope, snackbarHostState)
                { navController.navigate("CourseSelection") }
            }
            else {
                navController.navigate("CourseSelection")
            }
        }
        else {
            requestPermissionsLauncher.launch(permissions)
        }
    }

    Scaffold(
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "CommunityHub" ,
            "Events",
            R.string.home,
            R.string.community_hub,
            R.string.events,
            R.drawable.baseline_home_24,
            R.drawable.community_hub,
            R.drawable.events
        ) },
        containerColor = Color(0xFFF9FFF8),
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) {
        Box(modifier = Modifier
            .fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .systemBarsPadding(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Header()
                Spacer(modifier = Modifier.height(25.dp))
                ShotCaddieLogo()
                Spacer(modifier = Modifier.height(45.dp))
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Center
                ) {
                    Column(
                        modifier = Modifier.padding(5.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        HomeScreenButton(
                            { navController.navigate("Profile") },
                            painterResource(R.drawable.account),
                            stringResource(R.string.account)
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        HomeScreenButton(
                            { if (statisticsViewModel.courseNames.size < 1) {
                                statisticsViewModel.retrieveCourseNames(scope, snackbarHostState)
                            }
                                navController.navigate("GolfRoundSelection")
                            },
                            painterResource(R.drawable.baseline_query_stats_24),
                            stringResource(R.string.stats)
                        )
                    }
                    Column(
                        modifier = Modifier
                            .padding(5.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        HomeScreenButton(
                            { requestPermissions(locationService.permissions) },
                            painterResource(R.drawable.tee_off),
                            stringResource(R.string.tee_off)
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        HomeScreenButton(
                            { navController.navigate("Clubs") },
                            painterResource(R.drawable.stats),
                            stringResource(R.string.club_stats)
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        HomeScreenButton(
                            { navController.navigate("CommunityHub") },
                            painterResource(R.drawable.community_hub),
                            stringResource(R.string.community_hub)
                        )
                    }
                    Column(
                        modifier = Modifier
                            .padding(5.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        HomeScreenButton(
                            { navController.navigate("EquipmentList") },
                            painterResource(R.drawable.shop),
                            stringResource(R.string.shop)
                        )
                        Spacer(modifier = Modifier.height(10.dp))
                        HomeScreenButton(
                            { navController.navigate("Events") },
                            painterResource(R.drawable.events),
                            stringResource(R.string.events)
                        )
                    }
                }
            }
            if (AppState.currentState.value.showGolfTips.value) {
                GolfTipPopUp()
            }
        }
    }
}

@Composable
fun Header() {
    Box(modifier = Modifier
        .fillMaxWidth()
        .height(210.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.golf_background_image),
            contentDescription = "Golfer hitting the ball on the fairway",
            modifier = Modifier
                .fillMaxSize()
        )
    }
}

@Composable
fun HomeScreenButton(navigateTo: () -> Unit, image: Painter, description: String) {
    Button(
        onClick = navigateTo,
        shape = CircleShape,
        colors = ButtonDefaults.buttonColors(containerColor = GolfGreen),
        modifier = Modifier
            .size(70.dp)
            .shadow(8.dp, CircleShape, clip = true)
    ) {
        Icon(
            painter = image,
            contentDescription = description
        )
    }
    Text(
        text = description,
        modifier = Modifier.padding(5.dp),
        textAlign = TextAlign.Center,
        style = MaterialTheme.typography.bodyMedium
    )
}

@Composable
fun GolfTipPopUp() {
    Column(modifier = Modifier
        .padding(57.dp, 400.dp, 0.dp, 0.dp)
        .border(1.dp, Color.Black, RoundedCornerShape(4.dp))
        .background(color = GolfBlue, shape = RoundedCornerShape(4.dp))
        .width(300.dp)
        .height(150.dp),
        verticalArrangement = Arrangement.Center
    ) {
        val rand = (0..<AppState.currentState.value.golfTips.size).random()
        Text(modifier = Modifier
                .padding(5.dp, 5.dp, 5.dp, 5.dp),
            fontWeight = FontWeight.Bold,
            color = Color.White,
            text = "Daily Golf Tip")
        Text(modifier = Modifier
            .padding(5.dp, 0.dp, 5.dp, 5.dp),
            color = Color.White,
            textAlign = TextAlign.Center,
            text =  AppState.currentState.value.golfTips[rand]
        )
        Button(
            modifier = Modifier
                .padding(0.dp, 0.dp, 5.dp, 0.dp)
                .align(Alignment.End),
            shape = RoundedCornerShape(4.dp),
            colors = ButtonDefaults.buttonColors(containerColor = Color.White),
            onClick = { AppState.currentState.value.showGolfTips.value = false }
        ) {
            Text(
                text = stringResource(R.string.got_it),
                color = GolfBlue,
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }
}