package edu.vt.cs5934.golftracker.data

import com.google.firebase.Timestamp

data class Chats(
    val chatId: String = "",
    val user1: String = "",
    val user2: String = "",
    val lastMessageTimestamp: Timestamp = Timestamp.now(),
    var messages: List<Message> = emptyList()
) {
    constructor() : this("", "", "", Timestamp.now(), emptyList())
}