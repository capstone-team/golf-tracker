package edu.vt.cs5934.golftracker.service

import edu.vt.cs5934.golftracker.BuildConfig
import edu.vt.cs5934.golftracker.data.TournamentsResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query



interface SlashGolfApiService {
    companion object {
        private const val API_KEY_HEADER = "x-rapidapi-key: ${BuildConfig.SLASH_API_KEY}"
    }

    @GET("schedule")
    @Headers(
        API_KEY_HEADER,
        "x-rapidapi-host: live-golf-data.p.rapidapi.com"
    )
    suspend fun getSchedule(
        @Query("orgId") orgId: String,
        @Query("year") year: String
    ): TournamentsResponse
}