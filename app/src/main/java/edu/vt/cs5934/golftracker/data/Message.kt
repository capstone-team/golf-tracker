package edu.vt.cs5934.golftracker.data

import com.google.firebase.Timestamp

data class Message(
    val id: String = "",
    val senderId: String = "",
    val receiverId: String = "",
    val text: String = "",
    val timestamp: Timestamp = Timestamp.now()
) {
    constructor() : this("", "", "", "", Timestamp.now())
}