package edu.vt.cs5934.golftracker.viewmodels

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.SignInUIState
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.utils.mapToPair
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class SignInViewModel: MainAppViewModel() {

    var uiState = mutableStateOf(SignInUIState())
        private set

    fun onEmailChange(newValue: String) {
        uiState.value = uiState.value.copy(email = newValue)
    }

    fun onPasswordChange(newValue: String) {
        uiState.value = uiState.value.copy(password = newValue)
    }

    fun onSignIn(email: String, password: String, scope: CoroutineScope,
                 snackbarHostState: SnackbarHostState, actionToTake: () -> Unit) {
        Firebase.auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentUserId = accountService.currentUserId
                    firestore.collection("users").document(currentUserId)
                        .get()
                        .addOnSuccessListener { document ->
                            val obj = document.data
                            val userEmail = obj?.getValue("email").toString()
                            val userGuid = obj?.getValue("guid").toString()
                            val userHandicap = obj?.getValue("handicap").toString().toDouble()
                            val handicapDifferentials = obj?.getValue("handicapDifferentials") as MutableList<Double>
                            val lstOfClubs = obj.getValue("listOfClubs") as HashMap<String, HashMap<String, Any>>
                            val userName = obj.getValue("name").toString()

                            //convert from HashMap to GolfHoles
                            val listOfClubs: MutableMap<String, Pair<Int, Int>> =
                                mutableMapOf<String, Pair<Int, Int>>().apply {
                                    lstOfClubs.forEach { (s, pair) ->
                                        val newPair = mapToPair(pair)
                                        this[s] = newPair
                                    }
                                }

                            User.currentUser.value = User(userEmail, userGuid, userHandicap,
                                handicapDifferentials, listOfClubs, userName)
                        }
                    AppState.currentState.value.showGolfTips.value = true
                    actionToTake()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Invalid email or password")
                    }
                }
            }
    }

    fun hasUser(): Boolean {
        return accountService.hasUser()
    }
}