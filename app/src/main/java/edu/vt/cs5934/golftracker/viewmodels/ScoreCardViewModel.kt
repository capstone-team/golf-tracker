package edu.vt.cs5934.golftracker.viewmodels

import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.Course
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.data.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.Locale

class ScoreCardViewModel: MainAppViewModel() {

    val listOfHoleScores = GolfRound.golfRound.value.listOfHoleScores

    private val listOfGolfHoles = GolfRound.golfRound.value.listOfGolfHoles

    private fun updateToPar(): Int { return listOfGolfHoles.sumOf { it.par } }

    fun getRoundScore(): Int { return listOfHoleScores.sum() - updateToPar() }

    fun completeRound(
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
        navigateTo: () -> Unit
    ) {
        //add clubs to User
        GolfRound.golfRound.value.clubsUsed.forEach {club ->
            val newPair: Pair<Int, Int>
            if (User.currentUser.value.listOfClubs[club.key] != null) {
                val pair = User.currentUser.value.listOfClubs[club.key]
                val totalDistance = pair!!.first + club.value.first
                val count = pair.second + club.value.second
                newPair = Pair(totalDistance, count)
            }
            else {
                newPair = Pair(club.value.first, club.value.second)
            }
            User.currentUser.value.listOfClubs[club.key] = newPair
        }


        //update handicap and user clubs
        val roundDiff = calculateHandicapDifferential(Course.selectedCourse.value, listOfHoleScores.sum())
        User.currentUser.value.handicapDifferentials.add(roundDiff)
        val handicapIndex = updateHandicapIndex(User.currentUser.value.handicapDifferentials)

        firestore.collection("users").document(accountService.currentUserId)
            .update("handicapDifferentials", User.currentUser.value.handicapDifferentials,
                "handicap", handicapIndex, "listOfClubs", User.currentUser.value.listOfClubs
            )

        val roundScore = getRoundScore()
        GolfRound.golfRound.value.roundScore = getRoundScore()

        //update round information
        firestore.collection("users").document(User.currentUser.value.guid)
            .collection(GolfRound.golfRound.value.courseName).add(GolfRound.golfRound.value)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    scope.launch {
                        snackbarHostState.showSnackbar("You shot a round of $roundScore. " +
                                "Your handicap has been updated",
                            duration = SnackbarDuration.Long
                        )
                    }
                    GolfRound.golfRound.value.clubsUsed = mutableMapOf()
                    GolfRound.golfRound.value.courseName = ""
                    GolfRound.golfRound.value.date = ""
                    GolfRound.golfRound.value.roundScore = 0
                    GolfRound.golfRound.value.listOfGolfHoles.clear()
                    GolfRound.golfRound.value.listOfHoleScores.clear()
                    AppState.currentState.value.isRoundInProgress.value = false
                    navigateTo()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to save round. " +
                                "Please try again",
                        )
                    }
                }
            }
    }

    private fun calculateHandicapDifferential(selectedCourse: Course, grossScore: Int):Double {
        return String.format(Locale.US, "%.1f",
            (((grossScore - selectedCourse.courseRating) * 113) / selectedCourse.slope)).toDouble()
    }

    private fun updateHandicapIndex(listOfDifferentials: MutableList<Double>): Double {
        val sortedList = listOfDifferentials.sorted()
        val slice: List<Double>

        if (listOfDifferentials.size <= 6) {
            slice = sortedList.slice(0..0)
        }
        else if (listOfDifferentials.size <= 8) {
            slice = sortedList.slice(0..1)
        }
        else if (listOfDifferentials.size <= 10) {
            slice = sortedList.slice(0..2)
        }
        else if (listOfDifferentials.size <= 12) {
            slice = sortedList.slice(0..3)
        }
        else if (listOfDifferentials.size <= 14) {
            slice = sortedList.slice(0..4)
        }
        else if (listOfDifferentials.size <= 16) {
            slice = sortedList.slice(0..5)
        }
        else if (listOfDifferentials.size == 17) {
            slice = sortedList.slice(0..6)
        }
        else if (listOfDifferentials.size == 18) {
            slice = sortedList.slice(0..7)
        }
        else if (listOfDifferentials.size == 19) {
            slice = sortedList.slice(0..8)
        }
        else {
            slice = sortedList.slice(0..9)
        }
        val avgDiffs = slice.sum() / slice.size

        val handicapIndex = String.format(Locale.US, "%.1f", (avgDiffs / .96)).toDouble()
        User.currentUser.value.handicap = handicapIndex

        return handicapIndex
    }
}