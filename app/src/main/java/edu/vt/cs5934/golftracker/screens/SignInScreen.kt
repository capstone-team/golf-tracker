package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Gray
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.viewmodels.SignInViewModel

@Composable
fun SignInScreen(
    navController: NavHostController,
    viewModel: SignInViewModel
) {

    val uiState by viewModel.uiState
    val showEmailError = remember { mutableStateOf(false) }
    val showSignInError = remember { mutableStateOf(false) }
    val showEmptyFieldsError = remember { mutableStateOf(false) }
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .padding(0.dp, 100.dp, 0.dp, 0.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,

            ) {
            ShotCaddieLogo()
            Spacer(modifier = Modifier.height(80.dp))
            SignInScreenGreeting()
            Spacer(modifier = Modifier.height(80.dp))
            OutlinedTextField(
                value = uiState.email,
                onValueChange = {
                    viewModel.onEmailChange(it)
                    showSignInError.value = false
                    showEmailError.value = false
                },
                label = { Text(text = stringResource(R.string.email)) },
                modifier = Modifier
                    .width(330.dp),
            )
            Spacer(modifier = Modifier.height(4.dp))
            if(showEmptyFieldsError.value) {
                EmptyFieldsError()
            }
            if(showEmailError.value) {
                EmailEntryError()
            }
            if(showSignInError.value) {
                SignInError()
            }
            OutlinedTextField(
                value = uiState.password,
                onValueChange = { viewModel.onPasswordChange(it)
                    showSignInError.value = false
                    showEmailError.value = false
                    showEmptyFieldsError.value = false
                },
                label = { Text(text = stringResource(R.string.password)) },
                modifier = Modifier
                    .width(330.dp),
                visualTransformation = PasswordVisualTransformation()
            )
            Spacer(modifier = Modifier.height(16.dp))
            Button(
                modifier = Modifier
                    .width(330.dp)
                    .height(48.dp),
                shape = RoundedCornerShape(4.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Black),
                onClick = {
                    if (uiState.email == "" || uiState.password == "") {
                        showEmptyFieldsError.value = true
                    } else if (!uiState.email.contains("@")) {
                        showEmailError.value = true
                    } else {
                        viewModel.onSignIn(uiState.email, uiState.password, scope, snackbarHostState) {
                            navController.navigate("Home") {
                                popUpTo("SignIn") { inclusive = true }
                            }
                        }
                    }
                },
            ) {
                Text(text = stringResource(R.string.sign_in),
                    style = MaterialTheme.typography.bodyLarge)
            }
            Spacer(modifier = Modifier.height(16.dp))
            TextButton(onClick = { navController.navigate("SignUp") }) {
                Text(
                    text = stringResource(R.string.no_account),
                    style = MaterialTheme.typography.bodySmall,
                    color = Gray,
                )
            }
        }
    }
}

@Composable
fun SignInScreenGreeting() {
    Column (
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.sign_in_greeting),
            style = MaterialTheme.typography.titleLarge
        )
        Spacer(modifier = Modifier.height(2.dp))
        Text(
            text = stringResource(R.string.sign_in_sub_greeting),
            style = MaterialTheme.typography.titleSmall,
            color = Gray
        )
    }
}

@Composable
fun ShotCaddieLogo() {
    Row {
        Text(
            text = stringResource(R.string.app_name_first),
            style = MaterialTheme.typography.headlineLarge,
            color = Color(0xFF012037)
        )
        Image(
            painter = painterResource(R.drawable.logo_icon),
            contentDescription = "small golf green with a red flag",
            modifier = Modifier
                .clip(CircleShape)
                .size(40.dp)
        )
        Text(
            text = stringResource(R.string.app_name_second),
            style = MaterialTheme.typography.headlineLarge,
            color = Color(0xFF012037
            )
        )
    }
}

@Composable
fun EmailField(value: String, onNewValue: (String) -> Unit) {
    OutlinedTextField(
        value = value,
        onValueChange = { onNewValue(it) },
        label = { Text(text = stringResource(R.string.email)) },
        modifier = Modifier
            .width(330.dp),
    )
}

@Composable
fun PasswordField(value: String, onNewValue: (String) -> Unit) {
    OutlinedTextField(
        value = value,
        onValueChange = { onNewValue(it) },
        label = { Text(text = stringResource(R.string.password)) },
        modifier = Modifier
            .width(330.dp),
        visualTransformation = PasswordVisualTransformation()
    )
}

@Composable
fun EmailEntryError() {
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = stringResource(R.string.email_error),
        style = MaterialTheme.typography.titleSmall,
        color = Red
    )
}

@Composable
fun EmptyFieldsError() {
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = stringResource(R.string.empty_fields_error),
        style = MaterialTheme.typography.titleSmall,
        color = Red
    )
}

@Composable
fun SignInError() {
    Spacer(modifier = Modifier.height(4.dp))
    Text(
        text = stringResource(R.string.sign_in_error),
        style = MaterialTheme.typography.titleSmall,
        color = Red
    )
}