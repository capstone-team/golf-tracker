package edu.vt.cs5934.golftracker.viewmodels

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import com.google.firebase.Firebase
import com.google.firebase.auth.auth
import edu.vt.cs5934.golftracker.data.SignUpUIState
import edu.vt.cs5934.golftracker.data.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class SignUpViewModel : MainAppViewModel() {

    var uiState = mutableStateOf(SignUpUIState())
        private set

    fun onEmailChange(newValue: String) {
        uiState.value = uiState.value.copy(email = newValue)
    }

    fun onPasswordChange(newValue: String) {
        uiState.value = uiState.value.copy(password = newValue)
    }

    fun onConfirmPasswordChange(newValue: String) {
        uiState.value = uiState.value.copy(confirmPassword = newValue)
    }

    fun onSignUp(email: String, password: String, scope: CoroutineScope,
                 snackbarHostState: SnackbarHostState, actionToTake: () -> Unit
    ) {
        Firebase.auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val currentUserId = Firebase.auth.currentUser?.uid.orEmpty()
                    if (currentUserId != "") {
                        val newUser = User(email, currentUserId)
                        firestore.collection("users").document(currentUserId).set(newUser)
                        User.currentUser.value = newUser
                    }
                    scope.launch {
                        snackbarHostState.showSnackbar("Account Created")
                    }
                    actionToTake()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Could not create account. Please try again")
                    }
                }
            }
            .addOnFailureListener {
                scope.launch {
                    snackbarHostState.showSnackbar("Could not create account. Please try again")
                }
            }
    }
}