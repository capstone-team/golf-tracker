package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.PopupProperties
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.GolfEvent
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfContainer
import edu.vt.cs5934.golftracker.ui.theme.GolfGray
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.ui.theme.GolfRed
import edu.vt.cs5934.golftracker.viewmodels.EventScheduleViewModel
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Composable
fun EventScheduleScreen(
    navController: NavHostController,
    vm: EventScheduleViewModel
) {
    var isRefreshing by remember { mutableStateOf(false) }
    val filteredEvents by vm.filteredEvents.collectAsState()
    // can't add latest version due to gradle implementation issues
    val swipeRefreshState = rememberSwipeRefreshState(isRefreshing = isRefreshing)
    val snackbarHostState = remember { SnackbarHostState() }
    var showLocationFilter by remember { mutableStateOf(false) }
    var locationInput by remember { mutableStateOf("") }

    Scaffold(
        bottomBar = {
            BottomNavBar(
                navController,
                "Home",
                "CommunityHub",
                "Events",
                R.string.home,
                R.string.community_hub,
                R.string.events,
                R.drawable.baseline_home_24,
                R.drawable.community_hub,
                R.drawable.events
            )
        },
        containerColor = GolfContainer,
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { _ ->
        Column(
            modifier = Modifier
                .systemBarsPadding()
                .padding(bottom = 80.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            EventsHeader(navController, vm, showLocationFilter, { showLocationFilter = it }, locationInput, { locationInput = it })
            SwipeRefresh(
                modifier = Modifier.weight(1f),
                state = swipeRefreshState,
                onRefresh = {
                    isRefreshing = true
                    vm.fetchTournaments()
                    isRefreshing = false
                }
            ) {
                LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    verticalArrangement = Arrangement.spacedBy(4.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    contentPadding = PaddingValues(horizontal = 16.dp, vertical = 16.dp)
                ) {
                    items(filteredEvents) { events ->
                        DateBanner(date = events.date)
                        events.events.entries.forEach { pair ->
                            EventCard(navController, pair.toPair(), vm)
                        }
                    }
                }
            }
        }
    }
    if (showLocationFilter) {
        AlertDialog(
            onDismissRequest = { showLocationFilter = false },
            title = { Text("Enter State Code") },
            text = {
                TextField(
                    value = locationInput,
                    onValueChange = { locationInput = it },
                    label = { Text("State Code (e.g., VA)") },
                    singleLine = true,
                    maxLines = 1
                )
            },
            confirmButton = {
                Button(
                    colors = ButtonDefaults.buttonColors(containerColor = GolfGreen),
                    onClick = {
                        vm.setLocationFilter(locationInput)
                        vm.setFilterOption(EventScheduleViewModel.FilterOption.LOCATION)
                        showLocationFilter = false
                    }
                ) {
                    Text("Apply")
                }
            },
            dismissButton = {
                Button(
                    colors = ButtonDefaults.buttonColors(containerColor = GolfRed),
                    onClick = { showLocationFilter = false }) {
                    Text("Cancel")
                }
            }
        )
    }
}

@Composable
fun DateBanner(date: String) {
    val localDate = LocalDate.parse(date)
    val formattedDate = localDate.format(DateTimeFormatter.ofPattern("MMMM d, yyyy"))

    Box(
        modifier = Modifier
            .padding(vertical = 8.dp)
            .fillMaxWidth()
            .background(
                brush = Brush.horizontalGradient(
                    colors = listOf(GolfGreen, GolfGray)
                ),
                shape = RoundedCornerShape(8.dp)
            )
    ) {
        Text(
            text = formattedDate,
            modifier = Modifier
                .padding(12.dp)
                .align(Alignment.Center),
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.titleMedium.copy(
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        )
    }
}

@Composable
fun EventCard(
    navController: NavController,
    pair: Pair<String, GolfEvent>,
    vm: EventScheduleViewModel
) {
    ElevatedCard(
        onClick = {
            navController.navigate("EventForm")
            vm.currentEventPair = pair
        },
        modifier = Modifier
            .padding(vertical = 4.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(12.dp),
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
        colors = CardDefaults.cardColors(containerColor = Color.White)
    ) {
        Row(
            modifier = Modifier.padding(16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.tee_off),
                contentDescription = null,
                tint = GolfGreen,
                modifier = Modifier.size(40.dp)
            )
            Spacer(modifier = Modifier.width(16.dp))
            Column {
                Text(
                    text = pair.second.title,
                    style = MaterialTheme.typography.titleMedium,
                    color = Color.Black
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = formatTime(pair.second.startTime),
                    style = MaterialTheme.typography.bodyMedium,
                    color = Color.Gray
                )
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = pair.second.location,
                    style = MaterialTheme.typography.bodyMedium,
                    color = Color.Gray
                )
            }
        }
    }
}

fun formatTime(time: String): String {
    val formatter = DateTimeFormatter.ofPattern("HH:mm")
    val localTime = LocalTime.parse(time, formatter)
    return localTime.format(DateTimeFormatter.ofPattern("h:mm a"))
}

@Composable
fun EventsHeader(
    navController: NavHostController,
    vm: EventScheduleViewModel,
    showLocationFilter: Boolean,
    onShowLocationFilterChange: (Boolean) -> Unit,
    locationInput: String,
    onLocationInputChange: (String) -> Unit
) {
    var expanded by remember { mutableStateOf(false) }
    val currentFilter by vm.filterOption.collectAsState()
    val currentLocation by vm.locationFilter.collectAsState()
    val selectedText by remember(currentFilter, currentLocation) {
        derivedStateOf {
            when (currentFilter) {
                EventScheduleViewModel.FilterOption.ALL -> "All Events"
                EventScheduleViewModel.FilterOption.NATIONAL_TOURNAMENTS -> "National Tournaments"
                EventScheduleViewModel.FilterOption.MY_EVENTS -> "My Events"
                EventScheduleViewModel.FilterOption.LOCATION -> "Location: ${vm.locationFilter.value}"
            }
        }
    }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(GolfGreen, GolfGreen)
                )
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(
                onClick = { expanded = true },
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
            ) {
                Text(
                    text = selectedText,
                    style = MaterialTheme.typography.titleMedium,
                    color = Color.White
                )
                Icon(
                    painter = if (expanded) painterResource(R.drawable.up_arrow)
                    else painterResource(R.drawable.down_arrow),
                    contentDescription = stringResource(id = R.string.down_arrow),
                    tint = Color.White
                )
            }

            IconButton(
                onClick = {
                    val newTypeKey = vm.generateNewManualEventTypeKey()
                    val currentTime = LocalTime.now()
                    val timeFormatter = DateTimeFormatter.ofPattern("HH:mm")
                    val roundedTime = roundToNearestQuarter(currentTime)
                    var startTime = roundedTime.format(timeFormatter)
                    var endTime =roundedTime.plusHours(1).format(timeFormatter)
                    vm.currentEventPair = Pair(
                        newTypeKey, GolfEvent(
                            typeKey = newTypeKey,
                            startTime = startTime,
                            endTime = endTime
                        )
                    )
                    navController.navigate("EventForm")
                },
                modifier = Modifier.padding(8.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = stringResource(id = R.string.add_event),
                    tint = Color.White
                )
            }
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier
                .fillMaxWidth(0.8f)
                .background(Color.White)
                .align(Alignment.TopStart),
            properties = PopupProperties(focusable = true)
        ) {
            EventScheduleViewModel.FilterOption.entries.forEach { option ->
                DropdownMenuItem(
                    onClick = {
                        when (option) {
                            EventScheduleViewModel.FilterOption.LOCATION -> {
                                onShowLocationFilterChange(true)
                                vm.setFilterOption(option)
                            }

                            else -> {
                                vm.setFilterOption(option)
                            }
                        }
                        expanded = false
                    },
                    text = {
                        Row(
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            RadioButton(
                                selected = currentFilter == option,
                                onClick = null,
                                colors = RadioButtonDefaults.colors(selectedColor = GolfGreen)
                            )
                            Text(
                                when (option) {
                                    EventScheduleViewModel.FilterOption.ALL -> "All Events"
                                    EventScheduleViewModel.FilterOption.NATIONAL_TOURNAMENTS -> "National Tournaments"
                                    EventScheduleViewModel.FilterOption.MY_EVENTS -> "My Events"
                                    EventScheduleViewModel.FilterOption.LOCATION -> "Filter by Location"
                                }
                            )
                        }
                    }
                )
            }
        }
    }
}

