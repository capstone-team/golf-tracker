package edu.vt.cs5934.golftracker.viewmodels

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class EcommerceViewModel : ViewModel() {
    private val clubTypes = listOf("Driver", "Fairway", "Hybrid", "Iron", "Wedge", "Putter")

    private val clubUrls = mapOf(
        "Driver" to "https://www.rockbottomgolf.com/golf-clubs/drivers/",
        "Fairway" to "https://www.rockbottomgolf.com/golf-clubs/fairway-woods/",
        "Hybrid" to "https://www.rockbottomgolf.com/golf-clubs/hybrids/",
        "Iron" to "https://www.rockbottomgolf.com/golf-clubs/iron-sets/",
        "Wedge" to "https://www.rockbottomgolf.com/golf-clubs/wedges/",
        "Putter" to "https://www.rockbottomgolf.com/golf-clubs/putters/"
    )

    private val _selectedClubType = MutableStateFlow("Driver")
    val selectedClubType: StateFlow<String> = _selectedClubType.asStateFlow()

    fun getClubTypes(): List<String> = clubTypes

    fun getClubUrl(clubType: String): String = clubUrls[clubType] ?: clubUrls["Driver"]!!

    fun setSelectedClubType(clubType: String) {
        _selectedClubType.value = clubType
    }
}