package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.VerticalDivider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.Course
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.data.Hole
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfBlack
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.HoleViewModel
import edu.vt.cs5934.golftracker.viewmodels.ScoreCardViewModel

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ScoreCardScreen(
    navController: NavHostController,
    scoreCardViewModel: ScoreCardViewModel,
    holeViewModel: HoleViewModel,
) {
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }

    val courseName = GolfRound.golfRound.value.courseName
    val playerName = User.currentUser.value.name
    val courseScore = scoreCardViewModel.getRoundScore()

    var frontNineDistance = 0
    var backNineDistance = 0
    var frontNinePar = 0
    var backNinePar = 0

    val orderedListOfHoles = arrayOfNulls<Hole>(18).apply {
        for (hole in Course.selectedCourse.value.listOfHoles) {
            val i = hole.holeNumber
            this[i - 1] = hole

            if (i < 10) {
                frontNineDistance += hole.distance
                frontNinePar += hole.par
            } else {
                backNineDistance += hole.distance
                backNinePar += hole.par
            }
        }
    }

    val frontNineToggle = remember { mutableStateOf(scoreCardViewModel.listOfHoleScores.size <= 9) }

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) },
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "CommunityHub" ,
            "Course",
            R.string.home,
            R.string.community_hub,
            R.string.course_map,
            R.drawable.baseline_home_24,
            R.drawable.community_hub,
            R.drawable.baseline_map_24
        ) },
        containerColor = Color(0xFFFBFBFB)
    ) {
        Column(
            modifier = Modifier
                .padding(0.dp, 0.dp, 0.dp, 0.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.White)
                    .height(85.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    style = MaterialTheme.typography.titleLarge,
                    text = courseName
                )
            }
            HorizontalDivider(
                thickness = .4.dp,
                color = Color(0xFF262522)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = GolfBlack)
                    .height(80.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.padding(10.dp, 0.dp, 0.dp, 0.dp),
                    style = MaterialTheme.typography.bodyLarge,
                    fontWeight = FontWeight.Medium,
                    color = Color.White,
                    text = playerName
                )
                Text(
                    modifier = Modifier.padding(0.dp, 0.dp, 10.dp, 0.dp),
                    style = MaterialTheme.typography.bodyLarge,
                    fontWeight = FontWeight.Medium,
                    color = Color.White,
                    text = if (courseScore == 0) "E"
                        else if (courseScore > 0) "+$courseScore"
                        else courseScore.toString()
                )
            }
            HorizontalDivider(
                thickness = .4.dp,
                color = Color(0xFF262522)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.LightGray)
                    .padding(10.dp, 10.dp, 10.dp, 10.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    style = MaterialTheme.typography.bodyLarge,
                    text = stringResource(R.string.scorecard)
                )
                Button(
                    modifier = Modifier
                        .height(48.dp),
                    shape = RoundedCornerShape(4.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = Color.Unspecified),
                    onClick = { frontNineToggle.value = !frontNineToggle.value }
                ) {
                    if (!frontNineToggle.value) {
                        Icon(
                            painter = painterResource(R.drawable.baseline_navigate_before_24),
                            contentDescription = "previous arrow",
                        )
                    }
                    Text(
                        modifier = Modifier,
                        style = MaterialTheme.typography.bodyLarge,
                        text = if (frontNineToggle.value) stringResource(R.string.back_nine)
                        else stringResource(R.string.front_nine),
                    )
                    if (frontNineToggle.value) {
                        Icon(
                            painter = painterResource(R.drawable.baseline_navigate_next_24),
                            contentDescription = "next arrow",
                        )
                    }
                }
            }
            HorizontalDivider(
                thickness = .4.dp,
                color = Color(0xFF262522)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(400.dp)
                    .padding(10.dp, 0.dp, 10.dp, 0.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    if (frontNineToggle.value) {
                        for (num in 0..10) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        0 -> "Hole"
                                        10 -> "Out"
                                        else -> (num).toString()
                                    },
                                    fontWeight = if (num == 0) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                    if (!frontNineToggle.value) {
                        for (num in 10..20) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        10 -> "Hole"
                                        20 -> "In"
                                        else -> (num-1).toString()
                                    },
                                    fontWeight = if (num == 10) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                }
                VerticalDivider(
                    thickness = .4.dp,
                    color = Color(0xFF262522)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    if (frontNineToggle.value) {
                        for (num in 0..10) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        0 -> "Distance"
                                        10 -> frontNineDistance.toString()
                                        else -> orderedListOfHoles[num-1]!!.distance.toString()
                                    },
                                    fontWeight = if (num == 0) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                    if (!frontNineToggle.value) {
                        for (num in 10..20) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        10 -> "Distance"
                                        20 -> backNineDistance.toString()
                                        else -> orderedListOfHoles[num-2]!!.distance.toString()
                                    },
                                    fontWeight = if (num == 10) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                }
                VerticalDivider(
                    thickness = .4.dp,
                    color = Color(0xFF262522)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    if (frontNineToggle.value) {
                        for (num in 0..10) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        0 -> "Par"
                                        10 -> frontNinePar.toString()
                                        else -> orderedListOfHoles[num-1]!!.par.toString()
                                    },
                                    fontWeight = if (num == 0) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                    if (!frontNineToggle.value) {
                        for (num in 10..20) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    when (num) {
                                        10 -> "Par"
                                        20 -> backNinePar.toString()
                                        else -> orderedListOfHoles[num-2]!!.par.toString()
                                    },
                                    fontWeight = if (num == 10) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                }
                VerticalDivider(
                    thickness = .4.dp,
                    color = Color(0xFF262522)
                )
                val size = scoreCardViewModel.listOfHoleScores.size
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    if (frontNineToggle.value) {
                        for (num in 0..10) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    if (num == 0) "Score"
                                    else if (num != 10 && size >= num)
                                        scoreCardViewModel.listOfHoleScores[num-1].toString()
                                    else if (num == 10 && size > 0)
                                        if (size < 9)
                                            scoreCardViewModel.listOfHoleScores.slice(0..<size)
                                                .sum().toString()
                                        else
                                            scoreCardViewModel.listOfHoleScores.slice(0..8).sum()
                                                .toString()
                                    else "",

                                    fontWeight = if (num == 0) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                    if (!frontNineToggle.value) {
                        for (num in 10..20) {
                            Row(
                                modifier = Modifier
                                    .padding(10.dp, 10.dp, 10.dp, 0.dp),
                            ) {
                                Text(
                                    if (num == 10) "Score"
                                    else if (num != 20 && size >= num-1)
                                        scoreCardViewModel.listOfHoleScores[num - 2].toString()
                                    else if (num == 20 && size > 0)
                                        if (size < 18)
                                            scoreCardViewModel.listOfHoleScores.slice(9..<size)
                                                .sum().toString()
                                        else
                                            scoreCardViewModel.listOfHoleScores.slice(9..17).sum()
                                                .toString()
                                    else "",

                                    fontWeight = if (num == 10) FontWeight.Bold else FontWeight.Normal
                                )
                            }
                        }
                    }
                }
            }
            HorizontalDivider(
                modifier = Modifier
                    .padding(0.dp, 0.dp, 0.dp, 0.dp),
                thickness = .4.dp,
                color = Color(0xFF262522)
            )
            Column(modifier = Modifier
                .padding(0.dp, 0.dp, 0.dp, 0.dp)
                .background(color = GolfBlack)
                .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Spacer(modifier = Modifier.height(60.dp))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(10.dp, 0.dp, 10.dp, 0.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Button(
                        modifier = Modifier
                            .height(48.dp),
                        shape = RoundedCornerShape(4.dp),
                        colors = if (!AppState.currentState.value.isRoundInProgress.value) {
                            if (holeViewModel.replayHoleNumber.intValue < 2) {
                                ButtonDefaults.buttonColors(containerColor = Color.LightGray)
                            } else {
                                ButtonDefaults.buttonColors(containerColor = Color(0xFFEB9101))
                            }
                        } else {
                            ButtonDefaults.buttonColors(containerColor = Color(0xFFEB9101))
                        },
                        onClick = { if (!AppState.currentState.value.isRoundInProgress.value) {
                            if (holeViewModel.replayHoleNumber.intValue > 1) {
                                holeViewModel.decrementReplayHoleNumber()
                                navController.navigate("Hole")
                            }
                        } else {
                            scoreCardViewModel.completeRound(scope, snackbarHostState)
                            { navController.navigate("Home") {
                                popUpTo("Home") { inclusive = true }
                            } }
                        } }
                    ) {
                        Text(modifier = Modifier,
                            text = if (!AppState.currentState.value.isRoundInProgress.value) {
                                stringResource(R.string.prev_hole)
                            } else {
                                stringResource(R.string.end_round)
                            },
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                    Button(
                        modifier = Modifier
                            .height(48.dp),
                        shape = RoundedCornerShape(4.dp),
                        colors = if (!AppState.currentState.value.isRoundInProgress.value) {
                            if (holeViewModel.replayHoleNumber.intValue < 18) {
                                ButtonDefaults.buttonColors(containerColor = GolfGreen)
                            } else {
                                ButtonDefaults.buttonColors(containerColor = Color.LightGray)
                            }
                        } else {
                            if (scoreCardViewModel.listOfHoleScores.size < 18) ButtonDefaults.buttonColors(containerColor = GolfGreen)
                            else ButtonDefaults.buttonColors(containerColor = Color.LightGray)
                        },
                        onClick = { if (!AppState.currentState.value.isRoundInProgress.value) {
                            if (holeViewModel.replayHoleNumber.intValue < 18) {
                                holeViewModel.incrementReplayHoleNumber()
                                navController.navigate("Hole")
                            }
                        } else {
                            if (scoreCardViewModel.listOfHoleScores.size < 18) {
                                holeViewModel.currentHoleNumber.intValue = GolfRound.golfRound.value.listOfGolfHoles.size + 1
                                holeViewModel.isHoleInProgress.value = true
                                navController.navigate("Hole")
                            }
                        } }
                    ) {
                        Text(modifier = Modifier,
                            text = stringResource(R.string.next_hole),
                            style = MaterialTheme.typography.bodyLarge
                        )
                    }
                }
            }
        }
    }
}