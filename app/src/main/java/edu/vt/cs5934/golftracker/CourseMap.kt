package edu.vt.cs5934.golftracker

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MarkerComposable
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import edu.vt.cs5934.golftracker.data.Course
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.navigation.TopNavCourse

@Composable
fun CourseMap(
    navController: NavHostController
) {
    val selectedCourse = Course.selectedCourse

    val bounds = LatLngBounds(
        LatLng(selectedCourse.value.southWestBound.latitude, selectedCourse.value.southWestBound.longitude),
        LatLng(selectedCourse.value.northEastBound.latitude, selectedCourse.value.northEastBound.longitude)
    )

    val properties = MapProperties(
        latLngBoundsForCameraTarget = bounds,
        mapType = MapType.SATELLITE
    )
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(bounds.center, 15.9f)
    }
    val listOfHoles = selectedCourse.value.listOfHoles
    var isMapLoaded by remember { mutableStateOf(false) }

    Scaffold(
        topBar = { TopNavCourse(courseName = selectedCourse.value.name) },
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "Scorecard" ,
            "Hole",
            R.string.home,
            R.string.scorecard,
            R.string.hole_map,
            R.drawable.baseline_home_24,
            R.drawable.ic_score_icon,
            R.drawable.baseline_map_24
        ) }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            GoogleMap(
                modifier = Modifier.fillMaxSize(),
                onMapLoaded = { isMapLoaded = true },
                properties = properties,
                cameraPositionState = cameraPositionState,
            ) {
                listOfHoles.forEach { hole ->
                    val teeLocation = LatLng(hole.teeLocation.latitude, hole.teeLocation.longitude)
                    val centerOfGreen =
                        LatLng(hole.centerOfGreen.latitude, hole.centerOfGreen.longitude)

                    MarkerComposable(
                        state = rememberMarkerState(position = teeLocation),
                        title = "Hole ${hole.holeNumber}"
                    ) {
                        Image(
                            painter = painterResource(R.drawable.ic_golf_ball),
                            contentDescription = "golf ball on a tee",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(35.dp)
                        )
                    }
                    MarkerComposable(
                        state = rememberMarkerState(position = centerOfGreen),
                        title = "Green ${hole.holeNumber}"
                    ) {
                        Image(
                            painter = painterResource(R.drawable.ic_golf_flag),
                            contentDescription = "red golf flag",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(35.dp)
                        )
                    }
                }
            }
            //information sidebar
            Column {
                Column(
                    modifier = Modifier
                        .height(200.dp)
                        .background(Color.White.copy(alpha = .7f))
                        .width(90.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Column(
                        Modifier.padding(0.dp, 10.dp, 0.dp, 10.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                    ) {
                        Image(
                            painter = painterResource(R.drawable.ic_golf_course_icon),
                            contentDescription = "golf ball on a tee",
                            modifier = Modifier
                                .clip(CircleShape)
                                .size(35.dp)
                        )
                        Text(text = "Yards")
                        Text(
                            text = selectedCourse.value.distance.toString(),
                            style = MaterialTheme.typography.displaySmall,
                            color = Color(0xFF2B5620)
                        )
                    }
                    HorizontalDivider(
                        thickness = .4.dp,
                        color = Color(0xFF262522)
                    )
                    Column(
                        Modifier.padding(0.dp, 5.dp, 0.dp, 5.dp),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(text = "Par")
                        Text(
                            text = selectedCourse.value.par.toString(),
                            fontWeight = FontWeight.Bold
                        )
                    }
                }
                Button(
                    modifier = Modifier
                        .height(60.dp)
                        .width(90.dp)
                        .padding(0.dp, 5.dp, 0.dp, 0.dp),
                    shape = RectangleShape,
                    onClick = {
                        navController.navigate("Hole") {
                            popUpTo("Home") { inclusive = true }
                        }
                    },
                ) {
                    Column(horizontalAlignment = Alignment.CenterHorizontally) {
                        Icon(
                            modifier = Modifier.size(40.dp),
                            painter = painterResource(R.drawable.baseline_navigate_next_24),
                            contentDescription = "arrow",
                        )
                    }
                }
            }
        }
    }
}