package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.StatisticsViewModel

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun GolfRoundSelectionScreen(
    navController: NavHostController,
    statisticsViewModel: StatisticsViewModel
) {
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val selectedCourse = remember { mutableStateOf("") }
    val selectedDate = remember { mutableStateOf("") }
    val isCourseExpanded = remember { mutableStateOf(false) }
    val isDateExpanded = remember { mutableStateOf(false) }
    val courseIcon = if(isCourseExpanded.value) painterResource(R.drawable.baseline_arrow_drop_up_24)
        else painterResource(R.drawable.baseline_arrow_drop_down_24)
    val dateIcon = if(isDateExpanded.value) painterResource(R.drawable.baseline_arrow_drop_up_24)
        else painterResource(R.drawable.baseline_arrow_drop_down_24)

    Scaffold(
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) {
        Column(modifier = Modifier
            .padding(0.dp, 200.dp, 0.dp, 0.dp)
            .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Text(modifier = Modifier
                .padding(0.dp, 0.dp, 0.dp, 10.dp),
                text = stringResource(R.string.course_selection)
            )
            ExposedDropdownMenuBox(
                expanded = isCourseExpanded.value,
                onExpandedChange = { isCourseExpanded.value = !isCourseExpanded.value },
                modifier = Modifier
            ) {
                OutlinedTextField(
                    value = selectedCourse.value,
                    onValueChange = { selectedCourse.value = it },
                    label = { Text("Course") },
                    modifier = Modifier
                        .menuAnchor(),
                    colors = ExposedDropdownMenuDefaults.outlinedTextFieldColors(),
                    trailingIcon = {
                        Icon(
                            painter = courseIcon,
                            contentDescription = "arrow",
                            Modifier.clickable { isCourseExpanded.value = !isCourseExpanded.value }
                        )
                    }
                )
                ExposedDropdownMenu(
                    expanded = isCourseExpanded.value,
                    onDismissRequest = { isCourseExpanded.value = false }
                ) {
                    statisticsViewModel.courseNames.forEach { course ->
                        DropdownMenuItem(
                            text = {
                                Text(
                                    modifier = Modifier
                                        .align(Alignment.CenterHorizontally),
                                    text = course,
                                    textAlign = TextAlign.Center
                                )
                            },
                            onClick = {
                                selectedCourse.value = course
                                isCourseExpanded.value = false
                                statisticsViewModel.getRoundDates(selectedCourse.value, scope, snackbarHostState)
                            }
                        )
                    }
                }
            }
            if (selectedCourse.value != "") {
                Spacer(modifier = Modifier.height(100.dp))
                Text(modifier = Modifier
                    .padding(0.dp, 0.dp, 0.dp, 10.dp),
                    text = "Select a date"
                )
                ExposedDropdownMenuBox(
                    expanded = isDateExpanded.value,
                    onExpandedChange = { isDateExpanded.value = !isDateExpanded.value },
                    modifier = Modifier
                ) {
                    OutlinedTextField(
                        value = selectedDate.value,
                        onValueChange = { selectedDate.value = it },
                        label = { Text("Date") },
                        modifier = Modifier
                            .menuAnchor(),
                        colors = ExposedDropdownMenuDefaults.outlinedTextFieldColors(),
                        trailingIcon = {
                            Icon(
                                painter = dateIcon,
                                contentDescription = "arrow",
                                Modifier.clickable { isDateExpanded.value = !isDateExpanded.value }
                            )
                        }
                    )
                    ExposedDropdownMenu(
                        expanded = isDateExpanded.value,
                        onDismissRequest = { isDateExpanded.value = false }
                    ) {
                        statisticsViewModel.datesPlayed.forEach { date ->
                            DropdownMenuItem(
                                text = {
                                    Text(
                                        modifier = Modifier
                                            .align(Alignment.CenterHorizontally),
                                        text = date,
                                        textAlign = TextAlign.Center
                                    )
                                },
                                onClick = {
                                    selectedDate.value = date
                                    isDateExpanded.value = false
                                }
                            )
                        }
                    }
                }
                Spacer(modifier = Modifier.height(100.dp))
                Button(
                    modifier = Modifier
                        .size(150.dp),
                    shape = CircleShape,
                    colors = if (selectedCourse.value != "" && selectedDate.value != "") ButtonDefaults.buttonColors(containerColor = GolfGreen)
                    else ButtonDefaults.buttonColors(containerColor = Color.LightGray),
                    onClick = { statisticsViewModel.retrieveRoundAndCourse(selectedCourse.value, selectedDate.value,
                        scope, snackbarHostState) {navController.navigate("Statistics")} },
                ) {
                    Text(modifier = Modifier,
                        text = stringResource(R.string.get_round),
                        style = MaterialTheme.typography.bodyLarge
                    )
                }
            }
        }
    }
}