package edu.vt.cs5934.golftracker.data

import com.google.firebase.firestore.GeoPoint

data class Hole(
    var centerOfGreen: GeoPoint,
    var courseId: Int,
    var distance: Int,
    var holeNumber: Int,
    var northEastBound: GeoPoint,
    var par: Int,
    var southWestBound: GeoPoint,
    var teeLocation: GeoPoint
)
