package edu.vt.cs5934.golftracker.data

data class GolfEvents(
    val date: String,
    val events: MutableMap<String, GolfEvent>
)
