package edu.vt.cs5934.golftracker.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

data class Course(val id: Int, val name: String, val url: String)

class EventTeeTimesViewModel : ViewModel() {
    private val firestore = FirebaseFirestore.getInstance()

    private val _courses = MutableStateFlow<List<Course>>(emptyList())
    val courses: StateFlow<List<Course>> = _courses

    private val _selectedCourse = MutableStateFlow<Course?>(null)
    val selectedCourse: StateFlow<Course?> = _selectedCourse

    private val _selectedUrl = MutableStateFlow<String>("https://www.golfpass.com/travel-advisor/courses/12927-greendale-golf-course")
    val selectedUrl: StateFlow<String> = _selectedUrl

    private val _errorMessage = MutableStateFlow<String?>(null)
    val errorMessage: StateFlow<String?> = _errorMessage

    init {
        loadCourses()
    }

    private fun loadCourses() {
        viewModelScope.launch {
            try {
                val courseNames = firestore.collection("courseNames").get().await()
                val courseList = mutableListOf<Course>()

                for (document in courseNames.documents) {
                    val courseId = document.getLong("courseId")?.toInt() ?: continue
                    val name = document.getString("name") ?: continue

                    val urlDoc = firestore.collection("courseUrl").document(courseId.toString()).get().await()
                    val url = urlDoc.getString("url") ?: continue

                    courseList.add(Course(courseId, name, url))
                }

                _courses.value = courseList

                // Select the first course as default if the list is not empty
                if (courseList.isNotEmpty()) {
                    selectCourse(courseList.first())
                } else {
                    _errorMessage.value = "No courses found"
                }
            } catch (e: Exception) {
                _errorMessage.value = "Failed to load courses: ${e.localizedMessage}"
                e.printStackTrace()
            }
        }
    }

    fun selectCourse(course: Course) {
        _selectedCourse.value = course
        _selectedUrl.value = course.url
    }
}