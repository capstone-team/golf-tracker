package edu.vt.cs5934.golftracker.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import edu.vt.cs5934.golftracker.CourseMap
import edu.vt.cs5934.golftracker.HoleMap
import edu.vt.cs5934.golftracker.screens.ClubScreen
import edu.vt.cs5934.golftracker.screens.ChatListScreen
import edu.vt.cs5934.golftracker.screens.ChatScreen
import edu.vt.cs5934.golftracker.screens.CourseSelectionScreen
import edu.vt.cs5934.golftracker.viewmodels.CourseViewModel
import edu.vt.cs5934.golftracker.screens.CreateAccountScreen
import edu.vt.cs5934.golftracker.screens.EcommerceListScreen
import edu.vt.cs5934.golftracker.screens.EventFormScreen
import edu.vt.cs5934.golftracker.screens.EventHomeScreen
import edu.vt.cs5934.golftracker.screens.EventLiveScoresScreen
import edu.vt.cs5934.golftracker.screens.EventScheduleScreen
import edu.vt.cs5934.golftracker.screens.EventTeeTimesScreen
import edu.vt.cs5934.golftracker.screens.GolfRoundSelectionScreen
import edu.vt.cs5934.golftracker.screens.HomeScreen
import edu.vt.cs5934.golftracker.screens.ProfileScreen
import edu.vt.cs5934.golftracker.screens.ScoreCardScreen
import edu.vt.cs5934.golftracker.screens.SignInScreen
import edu.vt.cs5934.golftracker.screens.SignUpScreen
import edu.vt.cs5934.golftracker.screens.StatisticsScreen
import edu.vt.cs5934.golftracker.viewmodels.ChatListViewModel
import edu.vt.cs5934.golftracker.viewmodels.EcommerceViewModel
import edu.vt.cs5934.golftracker.viewmodels.EventTeeTimesViewModel
import edu.vt.cs5934.golftracker.viewmodels.EventScheduleViewModel
import edu.vt.cs5934.golftracker.viewmodels.HoleViewModel
import edu.vt.cs5934.golftracker.viewmodels.ProfileViewModel
import edu.vt.cs5934.golftracker.viewmodels.ScoreCardViewModel
import edu.vt.cs5934.golftracker.viewmodels.SignInViewModel
import edu.vt.cs5934.golftracker.viewmodels.SignUpViewModel
import edu.vt.cs5934.golftracker.viewmodels.StatisticsViewModel

@Composable
fun NavigationGraph(
    signInViewModel: SignInViewModel,
    signUpViewModel: SignUpViewModel,
    profileViewModel: ProfileViewModel,
    courseViewModel: CourseViewModel,
    holeViewModel: HoleViewModel,
    eventScheduleViewModel: EventScheduleViewModel,
    eventTeeTimesViewModel: EventTeeTimesViewModel,
    chatListViewModel: ChatListViewModel,
    ecommerceViewModel: EcommerceViewModel,
    scoreCardViewModel: ScoreCardViewModel,
    statisticsViewModel: StatisticsViewModel,
    startDestination: String,
    ) {

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = startDestination) {
        composable("SignIn") {
            SignInScreen(navController, signInViewModel)
        }

        composable(route = "SignUp") {
            SignUpScreen(navController, signUpViewModel)
        }

        composable(route = "CreateAccount") {
            CreateAccountScreen(navController, signUpViewModel)
        }

        composable(route = "Home") {
            HomeScreen(navController, holeViewModel, courseViewModel, statisticsViewModel)
        }

        composable(route = "Profile") {
            ProfileScreen(navController, profileViewModel)
        }

        composable(route = "Course") {
            CourseMap(navController)
        }

        composable(route = "Hole") {
            HoleMap(navController, holeViewModel)
        }

        composable(route = "CommunityHub") {
            ChatListScreen(navController, chatListViewModel)
        }

        composable(
            route = "Chat/{otherUserId}/{otherUserName}",
            arguments = listOf(
                navArgument("otherUserId") { type = NavType.StringType },
                navArgument("otherUserName") { type = NavType.StringType }
            )
        ) { backStackEntry ->
            val otherUserId = backStackEntry.arguments?.getString("otherUserId") ?: ""
            val otherUserName = backStackEntry.arguments?.getString("otherUserName") ?: ""
            ChatScreen(navController, otherUserId, otherUserName)
        }

        composable(route = "EquipmentList") {
            EcommerceListScreen(navController, ecommerceViewModel)
        }

        composable(route = "Scorecard") {
            ScoreCardScreen(navController, scoreCardViewModel, holeViewModel)
        }

        composable(route = "EventSchedule") {
            EventScheduleScreen(navController, eventScheduleViewModel)
        }

        composable(route = "EventForm") {
            EventFormScreen(navController, eventScheduleViewModel)
        }

        composable(route = "EventTeeTime") {
            EventTeeTimesScreen(navController, eventTeeTimesViewModel)
        }
        composable(route = "EventLiveScore") {
            EventLiveScoresScreen(navController)
        }
        composable(route = "Events") {
            EventHomeScreen(navController)
        }

        composable(route = "CourseSelection") {
            CourseSelectionScreen(navController, courseViewModel)
        }

        composable(route = "GolfRoundSelection") {
            GolfRoundSelectionScreen(navController, statisticsViewModel)
        }

        composable(route = "Statistics") {
            StatisticsScreen(navController, statisticsViewModel)
        }

        composable(route = "Clubs") {
            ClubScreen(navController)
        }
    }
}