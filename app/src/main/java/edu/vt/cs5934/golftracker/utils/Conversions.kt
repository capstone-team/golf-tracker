package edu.vt.cs5934.golftracker.utils

import com.google.android.gms.maps.model.LatLng
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint
import edu.vt.cs5934.golftracker.data.Ball
import edu.vt.cs5934.golftracker.data.GolfHole
import edu.vt.cs5934.golftracker.data.Hole
import edu.vt.cs5934.golftracker.data.User

fun metersToYards(meters: Double): Int {
    return (meters * 1.09361).toInt() // 1 meter = 1.09361 yards
}

// Mapper function to convert HashMap to GolfHole
fun mapToGolfHole(hashMap: HashMap<String, Any>): GolfHole {
    val lstOfBalls = hashMap["listOfBalls"] as MutableList<HashMap<String, Any>>
    val listOfBalls: MutableList<Ball> = mutableListOf<Ball>().apply {
        lstOfBalls.forEach { ball ->
            val golfBall = mapToBall(ball)
            this.add(golfBall)
        }
    }

    return GolfHole(
        holeNumber = (hashMap["holeNumber"] as? Long)?.toInt() ?: 0,
        holeScore = (hashMap["holeScore"] as? Long)?.toInt() ?: 0,
        listOfBalls = listOfBalls, //hashMap["listOfBalls"] as MutableList<Ball>,
        numOfPutts = (hashMap["numOfPutts"] as? Long)?.toInt() ?: 0,
        par = (hashMap["par"] as? Long)?.toInt() ?: 0,
    )
}

// Mapper function to convert HashMap to Ball
fun mapToBall(hashMap: HashMap<String, Any>): Ball {
    val latlng = hashMap["location"] as HashMap<String, Any>
    val location = mapToLatLng(latlng)

    return Ball(
        clubUsed = (hashMap["clubUsed"]).toString(),
        dateTime = (hashMap["dateTime"]).toString(),
        distance = (hashMap["distance"] as? Long)?.toInt() ?: 0,
        holeNumber = (hashMap["holeNumber"] as? Long)?.toInt() ?: 0,
        location = location,
        shotNumber = (hashMap["shotNumber"] as? Long)?.toInt() ?: 0
    )
}

// Mapper function to convert HashMap to LatLng
fun mapToLatLng(hashMap: HashMap<String, Any>): LatLng {
    return LatLng(
        (hashMap["latitude"] as? Double) ?: 0.0,
        (hashMap["longitude"] as? Double) ?: 0.0
    )
}

// Mapper function to convert HashMap to Hole
fun mapToHole(hashMap: HashMap<String, Any>): Hole {
    return Hole(
        centerOfGreen = hashMap["centerOfGreen"] as GeoPoint,
        courseId = hashMap["courseId"].toString().toInt(),
        distance = hashMap["distance"].toString().toInt(),
        holeNumber = hashMap["holeNumber"].toString().toInt(),
        northEastBound = hashMap["northEastBound"] as GeoPoint,
        par = hashMap["par"].toString().toInt(),
        southWestBound = hashMap["southWestBound"] as GeoPoint,
        teeLocation = hashMap["teeLocation"] as GeoPoint
    )
}

// Mapper function to convert HashMap to Hole
fun mapToPair(hashMap: HashMap<String, Any>): Pair<Int, Int> {
    val totalDistance = hashMap["first"].toString().toInt()
    val count = hashMap["second"].toString().toInt()
    return Pair(totalDistance, count)
}

fun docToUser(doc: DocumentSnapshot): User {
    val obj = doc.data
    val userEmail = obj?.getValue("email").toString()
    val userGuid = obj?.getValue("guid").toString()
    val userHandicap = obj?.getValue("handicap").toString().toDouble()
    val handicapDifferentials = obj?.getValue("handicapDifferentials") as MutableList<Double>
    val lstOfClubs = obj.getValue("listOfClubs") as HashMap<String, HashMap<String, Any>>
    val userName = obj.getValue("name").toString()

    //convert from HashMap to GolfHoles
    val listOfClubs: MutableMap<String, Pair<Int, Int>> =
        mutableMapOf<String, Pair<Int, Int>>().apply {
            lstOfClubs.forEach { (s, pair) ->
                val newPair = mapToPair(pair)
                this[s] = newPair
            }
        }

    return User(userEmail, userGuid, userHandicap,
        handicapDifferentials, listOfClubs, userName)
}