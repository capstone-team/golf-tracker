package edu.vt.cs5934.golftracker.data

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf

data class AppState(
    val golfTips: MutableList<String> = mutableListOf(),
    val isHoleInProgress: MutableState<Boolean> = mutableStateOf(false),
    val isRoundInProgress: MutableState<Boolean> = mutableStateOf(false),
    val showGolfTips: MutableState<Boolean> = mutableStateOf(false),
    val isCourseLoaded: MutableState<Boolean> = mutableStateOf(false)
) {
    companion object {
        val currentState = mutableStateOf(AppState())
    }
}