package edu.vt.cs5934.golftracker.data

data class ChatNotification(
    val title: String,
    val body: String,
    val chatId: String,
    val messageId: String,
    val senderId: String,
    val senderName: String
)