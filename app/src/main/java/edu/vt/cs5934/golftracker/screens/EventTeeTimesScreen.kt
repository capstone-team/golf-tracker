package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.RadioButton
import androidx.compose.material3.RadioButtonDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.EventTeeTimesViewModel

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun EventTeeTimesScreen(
    navController: NavHostController,
    vm: EventTeeTimesViewModel
) {
    val courses by vm.courses.collectAsState()
    val selectedUrl by vm.selectedUrl.collectAsState()
    val selectedCourse by vm.selectedCourse.collectAsState()
    val errorMessage by vm.errorMessage.collectAsState()
    val snackbarHostState = remember { SnackbarHostState() }
    var hasError by remember { mutableStateOf(false) }
    var expanded by remember { mutableStateOf(false) }

    LaunchedEffect(errorMessage) {
        errorMessage?.let {
            snackbarHostState.showSnackbar(it)
        }
    }

    Scaffold(
        bottomBar = {
            BottomNavBar(
                navController,
                "Home",
                "CommunityHub",
                "Events",
                R.string.home,
                R.string.community_hub,
                R.string.events,
                R.drawable.baseline_home_24,
                R.drawable.community_hub,
                R.drawable.events
            )
        },
        containerColor = Color(0xFFF9FFF8),
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues)
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(
                        brush = Brush.verticalGradient(
                            colors = listOf(GolfGreen, GolfGreen)
                        )
                    )
            ) {
                Button(
                    onClick = { expanded = true },
                    colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                ) {
                    Text(
                        text = selectedCourse?.name ?: "Select a course",
                        style = MaterialTheme.typography.titleMedium,
                        color = Color.White
                    )
                    Icon(
                        painter = if (expanded) painterResource(R.drawable.up_arrow)
                        else painterResource(R.drawable.down_arrow),
                        contentDescription = stringResource(id = R.string.down_arrow),
                        tint = Color.White
                    )
                }

                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                    modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .background(Color.White)
                ) {
                    courses.forEach { course ->
                        DropdownMenuItem(
                            onClick = {
                                vm.selectCourse(course)
                                expanded = false
                            },
                            text = {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically
                                ) {
                                    RadioButton(
                                        selected = selectedCourse == course,
                                        onClick = null,
                                        colors = RadioButtonDefaults.colors(selectedColor = GolfGreen)
                                    )
                                    Text(course.name)
                                }
                            }
                        )
                    }
                }
            }

            Box(modifier = Modifier.weight(1f)) {
                AndroidView(
                    factory = { context ->
                        WebView(context).apply {
                            webViewClient = object : WebViewClient() {
                                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                                    hasError = false
                                }

                                override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                                    hasError = true
                                }
                            }
                            settings.apply {
                                javaScriptEnabled = true
                                domStorageEnabled = true
                                loadsImagesAutomatically = true
                                useWideViewPort = true
                                loadWithOverviewMode = true
                            }
                        }
                    },
                    update = { webView ->
                        webView.loadUrl(selectedUrl)
                    },
                    modifier = Modifier.fillMaxSize()
                )

                if (hasError) {
                    Text(
                        text = "Failed to load the page. Please try again.",
                        color = Color.Red,
                        modifier = Modifier
                            .align(Alignment.Center)
                            .padding(16.dp)
                    )
                }
            }
        }
    }
}