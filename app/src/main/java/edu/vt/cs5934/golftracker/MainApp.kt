package edu.vt.cs5934.golftracker

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context.NOTIFICATION_SERVICE
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import edu.vt.cs5934.golftracker.navigation.NavigationGraph
import edu.vt.cs5934.golftracker.ui.theme.GolfTrackerTheme
import edu.vt.cs5934.golftracker.viewmodels.ChatListViewModel
import edu.vt.cs5934.golftracker.viewmodels.CourseViewModel
import edu.vt.cs5934.golftracker.viewmodels.EcommerceViewModel
import edu.vt.cs5934.golftracker.viewmodels.EventTeeTimesViewModel
import edu.vt.cs5934.golftracker.viewmodels.EventScheduleViewModel
import edu.vt.cs5934.golftracker.viewmodels.HoleViewModel
import edu.vt.cs5934.golftracker.viewmodels.MainAppViewModel
import edu.vt.cs5934.golftracker.viewmodels.ProfileViewModel
import edu.vt.cs5934.golftracker.viewmodels.ScoreCardViewModel
import edu.vt.cs5934.golftracker.viewmodels.SignInViewModel
import edu.vt.cs5934.golftracker.viewmodels.SignUpViewModel
import edu.vt.cs5934.golftracker.viewmodels.StatisticsViewModel

@Composable
fun MainApp() {

    val signInViewModel = SignInViewModel()
    val signUpViewModel = SignUpViewModel()
    val profileViewModel = ProfileViewModel()
    val courseViewModel = CourseViewModel()
    val holeViewModel = HoleViewModel()
    val eventScheduleViewModel = EventScheduleViewModel()
    val eventTeeTimesViewModel = EventTeeTimesViewModel()
    val chatListViewModel = ChatListViewModel()
    val ecommerceViewModel = EcommerceViewModel()
    val scoreCardViewModel = ScoreCardViewModel()
    val mainAppViewModel = MainAppViewModel()
    val statisticsViewModel = StatisticsViewModel()

    mainAppViewModel.updateUser()
    mainAppViewModel.getGolfTips()

    val context = LocalContext.current
    val notificationManager = context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

    val channel = NotificationChannel(
        "markBall",
        stringResource(R.string.mark_ball),
        NotificationManager.IMPORTANCE_DEFAULT
    )
    channel.description = "Notification to mark ball"
    notificationManager.createNotificationChannel(channel)

    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        GolfTrackerTheme{
            NavigationGraph(
                signInViewModel,
                signUpViewModel,
                profileViewModel,
                courseViewModel,
                holeViewModel,
                eventScheduleViewModel,
                eventTeeTimesViewModel,
                chatListViewModel,
                ecommerceViewModel,
                scoreCardViewModel,
                statisticsViewModel,
                startDestination = if (signInViewModel.hasUser()) "Home" else "SignIn" )
        }
    }
}