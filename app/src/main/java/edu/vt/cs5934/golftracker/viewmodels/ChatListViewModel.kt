package edu.vt.cs5934.golftracker.viewmodels

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.*
import edu.vt.cs5934.golftracker.data.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import com.google.android.gms.tasks.Task
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine
import com.google.firebase.Timestamp
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import edu.vt.cs5934.golftracker.utils.PairAdapter

class ChatListViewModel : ViewModel() {

    private val firestore = FirebaseFirestore.getInstance()
    private val usersCollection = firestore.collection("users")
    private val pageSize = 20

    private val _users = MutableStateFlow<List<User>>(emptyList())
    val users: StateFlow<List<User>> = _users

    private val _blockedUserIds = MutableStateFlow<Set<String>>(emptySet())
    val blockedUserIds: StateFlow<Set<String>> = _blockedUserIds

    private val _activeConversationIds = MutableStateFlow<Set<String>>(emptySet())
    val activeConversationIds: StateFlow<Set<String>> = _activeConversationIds

    private val _notification = MutableStateFlow<ChatNotification?>(null)
    val notification: StateFlow<ChatNotification?> = _notification

    private var lastLoadedUser: User? = null

    private val customGson: Gson = GsonBuilder()
        .registerTypeAdapter(object : TypeToken<Pair<Int, Int>>() {}.type, PairAdapter())
        .create()

    init {
        fetchUsers()
        fetchBlockedUserIds()
        listenForNewMessages()
        fetchActiveConversations()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                if (currentUserId.isEmpty()) {
                    Log.e("ChatListViewModel", "Current user ID is empty")
                    return@launch
                }
                val currentUserHandicap = User.currentUser.value.handicap

                val userMetadata = getOrCreateUserMetadata(currentUserId)

                val blockedUsers = userMetadata.blockedUsers
                val activeConversations = userMetadata.activeConversations

                val activeConversationUsers = fetchActiveConversationUsers(activeConversations)

                val excludedUsers = blockedUsers + activeConversations
                val usersQuery = buildUsersQuery(currentUserHandicap, excludedUsers)

                addUsersSnapshotListener(usersQuery, currentUserId, activeConversationUsers)
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error in fetchUsers", e)
            }
        }
    }

    private suspend fun fetchActiveConversationUsers(activeConversations: List<String>): List<User> {
        return activeConversations.mapNotNull { userId ->
            suspendCoroutine<User?> { continuation ->
                usersCollection.document(userId).get()
                    .addOnSuccessListener { doc ->
                        val user = try {
                            val json = customGson.toJson(doc.data)
                            customGson.fromJson(json, User::class.java)
                        } catch (e: Exception) {
                            Log.e("ChatListViewModel", "Error deserializing user", e)
                            null
                        }
                        continuation.resume(user)
                    }
                    .addOnFailureListener { e ->
                        continuation.resumeWithException(e)
                    }
            }
        }
    }

    fun addActiveConversation(otherUserId: String) {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                val userMetadataRef = firestore.collection("userMetadata").document(currentUserId)

                userMetadataRef.update("activeConversations", FieldValue.arrayUnion(otherUserId))

                _activeConversationIds.update { it + otherUserId }

                _users.update { currentList ->
                    val user = currentList.find { it.guid == otherUserId }
                    if (user != null) {
                        listOf(user) + currentList.filter { it.guid != otherUserId }
                    } else {
                        currentList
                    }
                }

                Log.d("ChatListViewModel", "Active conversation with $otherUserId added")
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error adding active conversation", e)
            }
        }
    }

    private fun fetchActiveConversations() {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                if (currentUserId.isEmpty()) {
                    Log.e("ChatListViewModel", "Current user ID is empty")
                    return@launch
                }
                val userMetadata = getOrCreateUserMetadata(currentUserId)
                _activeConversationIds.value = userMetadata.activeConversations.toSet()
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error fetching active conversations", e)
            }
        }
    }

    private fun addUsersSnapshotListener(usersQuery: Query, currentUserId: String, activeConversationUsers: List<User>) {
        usersQuery.addSnapshotListener { snapshot, error ->
            if (error != null) {
                Log.e("ChatListViewModel", "Error fetching users", error)
                return@addSnapshotListener
            }

            if (snapshot != null) {
                val usersList = snapshot.documents.mapNotNull { doc ->
                    try {
                        val json = customGson.toJson(doc.data)
                        customGson.fromJson(json, User::class.java)
                    } catch (e: Exception) {
                        Log.e("ChatListViewModel", "Error deserializing user", e)
                        null
                    }
                }.filter { it.guid != currentUserId }

                _users.update { (activeConversationUsers + usersList).distinctBy { it.guid } }
                lastLoadedUser = usersList.lastOrNull()
            }
        }
    }

    private suspend fun getOrCreateUserMetadata(userId: String): UserMetadata {
        if (userId.isEmpty()) {
            throw IllegalArgumentException("User ID cannot be empty")
        }
        return suspendCoroutine { continuation ->
            val userMetadataRef = firestore.collection("userMetadata").document(userId)
            userMetadataRef.get()
                .addOnSuccessListener { snapshot ->
                    if (snapshot.exists()) {
                        continuation.resume(snapshot.toObject(UserMetadata::class.java) ?: createNewUserMetadata(userId, userMetadataRef))
                    } else {
                        continuation.resume(createNewUserMetadata(userId, userMetadataRef))
                    }
                }
                .addOnFailureListener { e ->
                    continuation.resumeWithException(e)
                }
        }
    }

    private fun createNewUserMetadata(userId: String, userMetadataRef: DocumentReference): UserMetadata {
        val newMetadata = UserMetadata(userId = userId)
        userMetadataRef.set(newMetadata)
        return newMetadata
    }

    private fun buildUsersQuery(currentUserHandicap: Double, excludedUsers: List<String>): Query {
        val lowerBound = (currentUserHandicap - 10).coerceAtLeast(0.0)
        val upperBound = currentUserHandicap + 10

        var query = usersCollection
            .whereGreaterThanOrEqualTo("handicap", lowerBound)
            .whereLessThanOrEqualTo("handicap", upperBound)
            .orderBy("handicap")
            .limit(pageSize.toLong())

        if (excludedUsers.isNotEmpty()) {
            query = query.whereNotIn("guid", excludedUsers)
        }

        return query
    }

    fun loadMoreUsers() {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                val currentUserHandicap = User.currentUser.value.handicap

                val userMetadata = getOrCreateUserMetadata(currentUserId)

                val blockedUsers = userMetadata.blockedUsers
                val activeConversations = userMetadata.activeConversations

                val excludedUsers = blockedUsers + activeConversations
                var query = buildUsersQuery(currentUserHandicap, excludedUsers)

                lastLoadedUser?.let { lastUser ->
                    query = query.startAfter(lastUser.handicap)
                }

                query.get().addOnSuccessListener { snapshot ->
                    val newUsers = snapshot.documents.mapNotNull { doc ->
                        try {
                            val json = customGson.toJson(doc.data)
                            customGson.fromJson(json, User::class.java)
                        } catch (e: Exception) {
                            Log.e("ChatListViewModel", "Error deserializing user", e)
                            null
                        }
                    }.filter { it.guid != currentUserId && it.guid !in excludedUsers }

                    _users.update { currentList ->
                        (currentList + newUsers).distinctBy { it.guid }
                    }

                    lastLoadedUser = newUsers.lastOrNull()
                }.addOnFailureListener { e ->
                    Log.e("ChatListViewModel", "Error loading more users", e)
                }
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error in loadMoreUsers", e)
            }
        }
    }

    fun blockUser(userToBlockId: String) {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                val userMetadataRef = firestore.collection("userMetadata").document(currentUserId)

                userMetadataRef.update("blockedUsers", FieldValue.arrayUnion(userToBlockId))

                _blockedUserIds.update { it + userToBlockId }

                Log.d("ChatListViewModel", "User $userToBlockId blocked successfully")
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error blocking user", e)
            }
        }
    }

    fun unblockUser(userToUnblockId: String) {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                val userMetadataRef = firestore.collection("userMetadata").document(currentUserId)

                userMetadataRef.update("blockedUsers", FieldValue.arrayRemove(userToUnblockId))

                _blockedUserIds.update { it - userToUnblockId }

                Log.d("ChatListViewModel", "User $userToUnblockId unblocked successfully")
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error unblocking user", e)
            }
        }
    }

    private fun fetchBlockedUserIds() {
        viewModelScope.launch {
            try {
                val currentUserId = User.currentUser.value.guid
                if (currentUserId.isEmpty()) {
                    Log.e("ChatListViewModel", "Current user ID is empty")
                    return@launch
                }
                val userMetadata = getOrCreateUserMetadata(currentUserId)
                _blockedUserIds.value = userMetadata.blockedUsers.toSet()
            } catch (e: Exception) {
                Log.e("ChatListViewModel", "Error fetching blocked user IDs", e)
            }
        }
    }

    private fun listenForNewMessages() {
        val currentUserId = User.currentUser.value.guid
        firestore.collection("chats")
            .where(Filter.or(
                Filter.equalTo("user1", currentUserId),
                Filter.equalTo("user2", currentUserId)
            ))
            .addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.e("ChatListViewModel", "Error listening for new messages", error)
                    return@addSnapshotListener
                }

                snapshot?.documentChanges?.forEach { change ->
                    val chatData = change.document.data
                    viewModelScope.launch {
                        try {
                            val messagesField = chatData["messages"]
                            when (messagesField) {
                                is List<*> -> {
                                    val messages = messagesField as? List<Map<String, Any>>
                                    if (!messages.isNullOrEmpty()) {
                                        val lastMessage = messages.last().toMessage()
                                        if (lastMessage.receiverId == currentUserId) {
                                            createNotification(lastMessage, change.document.id)
                                        }
                                    }
                                }
                                is DocumentReference -> {
                                    val messagesRef = messagesField as DocumentReference
                                    val lastMessageQuery = messagesRef.collection("messages")
                                        .orderBy("timestamp", Query.Direction.DESCENDING)
                                        .limit(1)
                                    val messagesSnapshot = lastMessageQuery.get().await()
                                    val lastMessage = messagesSnapshot.documents.firstOrNull()?.toObject(Message::class.java)
                                    if (lastMessage != null && lastMessage.receiverId == currentUserId) {
                                        createNotification(lastMessage, change.document.id)
                                    }
                                }
                                else -> {
                                    Log.e("ChatListViewModel", "Unexpected messages field type: ${messagesField?.javaClass}")
                                }
                            }
                        } catch (e: Exception) {
                            Log.e("ChatListViewModel", "Error processing messages", e)
                        }
                    }
                }
            }
    }

    private fun Map<String, Any>.toMessage(): Message {
        return Message(
            id = this["id"] as? String ?: "",
            senderId = this["senderId"] as? String ?: "",
            receiverId = this["receiverId"] as? String ?: "",
            text = this["text"] as? String ?: "",
            timestamp = this["timestamp"] as? Timestamp ?: Timestamp.now()
        )
    }

    private suspend fun createNotification(message: Message, chatId: String) {
        try {
            val senderDoc = firestore.collection("users").document(message.senderId).get().await()
            val senderName = senderDoc.getString("name") ?: "Unknown User"
            val notification = ChatNotification(
                title = "New Message from $senderName",
                body = message.text,
                chatId = chatId,
                messageId = message.id,
                senderId = message.senderId,
                senderName = senderName
            )
            _notification.value = notification
        } catch (e: Exception) {
            Log.e("ChatListViewModel", "Error creating notification", e)
        }
    }

    fun clearNotification() {
        _notification.value = null
    }

    private suspend fun <T> Task<T>.await(): T = suspendCoroutine { continuation ->
        addOnSuccessListener { result ->
            continuation.resume(result)
        }
        addOnFailureListener { exception ->
            continuation.resumeWithException(exception)
        }
    }
}