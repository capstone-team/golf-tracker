package edu.vt.cs5934.golftracker.data

data class SignInUIState(
    val email: String = "",
    val password: String = "",
)