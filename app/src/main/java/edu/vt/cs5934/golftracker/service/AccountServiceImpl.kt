package edu.vt.cs5934.golftracker.service

import android.annotation.SuppressLint
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.firestore.auth.User
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
/*
Attribution given to Marina Coelho at Google Firebase for part of the code for interacting with Google's
Firebase. The Github repository is at https://github.com/FirebaseExtended/firebase-video-samples
*/

class AccountServiceImpl @Inject constructor() : AccountService {

    override val currentUser: Flow<User?>
        @SuppressLint("RestrictedApi")
        get() = callbackFlow {
            val listener =
                FirebaseAuth.AuthStateListener { auth ->
                    this.trySend(auth.currentUser?.let { User(it.uid) })
                }
            Firebase.auth.addAuthStateListener(listener)
            awaitClose { Firebase.auth.removeAuthStateListener(listener) }
        }

    override val currentUserId: String
        get() = Firebase.auth.currentUser?.uid.orEmpty()

    override fun hasUser(): Boolean {
        return Firebase.auth.currentUser != null
    }

    override suspend fun signIn(email: String, password: String) {
        Firebase.auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = Firebase.auth.currentUser
                }
            }.await()
    }

    override suspend fun signUp(email: String, password: String) { //.await()
        Firebase.auth.createUserWithEmailAndPassword(email, password)
    }

    override suspend fun signOut() {
        Firebase.auth.signOut()
    }

    override suspend fun deleteAccount() {
        Firebase.auth.currentUser!!.delete()//.await()
    }
}