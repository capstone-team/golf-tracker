package edu.vt.cs5934.golftracker.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val GolfRed = Color(0xFFFF4C4C)
val GolfBlack = Color(0xFF2D2D2D)
val GolfGreen = Color(0xFF2B5620)
val GolfBlue = Color(0xFF012037)
val GolfGray = Color(0xFF262522)
val PieChartGreen = Color(0xFF63C44B)
val PieChartBlue = Color(0xFF279AF0)
val GolfContainer = Color(0xFFF9FFF8)
