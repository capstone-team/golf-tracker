package edu.vt.cs5934.golftracker.viewmodels

import android.content.ContentValues.TAG
import android.util.Log
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.lifecycle.viewModelScope
import edu.vt.cs5934.golftracker.data.User
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class ProfileViewModel : MainAppViewModel() {

    fun onSignOut() {
        viewModelScope.launch {
            accountService.signOut()
        }
    }

    fun deleteAccount(scope: CoroutineScope, snackbarHostState: SnackbarHostState, actionToTake: () -> Unit) {
        scope.launch {
            val snackbarResult = snackbarHostState.showSnackbar(
                message = "Do you want to delete your account?",
                actionLabel = "Confirm",
                duration = SnackbarDuration.Short
            )
            when (snackbarResult) {
                SnackbarResult.ActionPerformed -> deleteDataAndNavigate(actionToTake)
                SnackbarResult.Dismissed -> Log.d(TAG,"Dismissed")
            }
        }
    }

    private fun deleteDataAndNavigate(actionToTake: () -> Unit) {
        firestore.collection("users").document(accountService.currentUserId)
            .delete().addOnSuccessListener {
                viewModelScope.launch {
                    accountService.deleteAccount()
                    actionToTake()
                }
            }
    }

    private fun profileHasChanged(name: String, handicap: String, email: String): Boolean {
        return User.currentUser.value.name != name
                || User.currentUser.value.handicap.toString() != handicap
                || User.currentUser.value.email != email
    }

    fun editOnClick(editMode: Boolean, name: String, handicap: String, email: String) {
        if (!editMode) {
            if (profileHasChanged(name, handicap, email)) {
                User.currentUser.value.name = name
                User.currentUser.value.handicap = handicap.toDouble()
                User.currentUser.value.email = email

                firestore.collection("users").document(accountService.currentUserId)
                    .update("name", name,
                        "handicap", handicap.toDouble(),
                        "email", email
                    )
            }
        }
    }
}