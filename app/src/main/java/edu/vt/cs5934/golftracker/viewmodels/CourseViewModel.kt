package edu.vt.cs5934.golftracker.viewmodels

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.mutableStateOf
import com.google.firebase.firestore.GeoPoint
import edu.vt.cs5934.golftracker.data.AppState
import edu.vt.cs5934.golftracker.data.Course
import edu.vt.cs5934.golftracker.data.GolfRound
import edu.vt.cs5934.golftracker.data.Hole
import edu.vt.cs5934.golftracker.utils.mapToHole
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class CourseViewModel: MainAppViewModel() {

    val courseNames = mutableListOf<String>()
    val selectedCourse = mutableStateOf(Course(0, 0.0, 0, listOf(),
        "", GeoPoint(0.0, 0.0), 0, 0, GeoPoint(0.0, 0.0))
    )

    fun retrieveCourseNames(
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
        navigateTo: () -> Unit
    ) {
        firestore.collection("courseNames")
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result) {
                        val obj = document.data
                        val index = obj.getValue("courseId").toString().toInt()
                        val name = obj.getValue("name").toString()
                        courseNames.add(index - 1, name)
                    }
                    navigateTo()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to retrieve courses. " +
                                "Restart and try again")
                    }
                }
            }
    }

    fun retrieveAndLoadCourse(
        courseName: String,
        scope: CoroutineScope,
        snackbarHostState: SnackbarHostState,
        roundInProgress: Boolean,
        navigateTo: () -> Unit
    ) {
        //reset GolfRound object
        GolfRound.golfRound.value.clubsUsed = mutableMapOf()
        GolfRound.golfRound.value.courseName = ""
        GolfRound.golfRound.value.date = ""
        GolfRound.golfRound.value.roundScore = 0
        GolfRound.golfRound.value.listOfGolfHoles.clear()
        GolfRound.golfRound.value.listOfHoleScores.clear()

        firestore.collection("courses")
            .whereEqualTo("name", courseName)
            .get()
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    for (document in it.result) {
                        val obj = document.data
                        val courseId = obj.getValue("courseId").toString().toInt()
                        val courseRating = obj.getValue("courseRating").toString().toDouble()
                        val distance = obj.getValue("distance").toString().toInt()
                        val lstOfHoles = obj.getValue("listOfHoles") as MutableList<HashMap<String, Any>>
                        val name = obj.getValue("name").toString()
                        val northEastBound = obj.getValue("northEastBound") as GeoPoint
                        val par = obj.getValue("par").toString().toInt()
                        val slope = obj.getValue("slope").toString().toInt()
                        val southWestBound = obj.getValue("southWestBound") as GeoPoint

                        //convert from HashMap to Holes
                        val listOfHoles = mutableListOf<Hole>().apply {
                            lstOfHoles.forEach{hole ->
                                val newHole = mapToHole(hole)
                                this.add(newHole)
                            }
                        }.toList()

                        selectedCourse.value = (Course(courseId, courseRating, distance, listOfHoles,
                            name, northEastBound, par, slope, southWestBound))
                    }
                    AppState.currentState.value.isCourseLoaded.value = true
                    AppState.currentState.value.isRoundInProgress.value = roundInProgress
                    GolfRound.golfRound.value.courseName = courseName
                    GolfRound.golfRound.value.date = currentDate
                    Course.selectedCourse.value = selectedCourse.value
                    navigateTo()
                }
                else {
                    scope.launch {
                        snackbarHostState.showSnackbar("Failed to retrieve course. " +
                                "Restart and try again")
                    }
                }
            }
    }
}