package edu.vt.cs5934.golftracker.data

data class GolfHole(
    var holeNumber: Int,
    var holeScore: Int = 0,
    var listOfBalls: MutableList<Ball> = mutableListOf(),
    var numOfPutts: Int = 0,
    var par: Int = 0
)