package edu.vt.cs5934.golftracker.data

import androidx.compose.runtime.mutableStateOf
import com.google.firebase.firestore.GeoPoint

data class Course(
    var courseId: Int = 0,
    var courseRating: Double = 0.0,
    var distance: Int = 0,
    var listOfHoles: List<Hole> = listOf(),
    var name: String = "",
    var northEastBound: GeoPoint = GeoPoint(0.0, 0.0),
    var par: Int = 0,
    var slope: Int = 0,
    var southWestBound: GeoPoint = GeoPoint(0.0, 0.0),
) {
    companion object {
        val selectedCourse = mutableStateOf(Course())
    }
}
