package edu.vt.cs5934.golftracker.data

import com.google.android.gms.maps.model.LatLng

data class Ball(
    var clubUsed: String = "",
    var dateTime: String = "",
    var distance: Int = 0,
    var holeNumber: Int,
    var location: LatLng,
    var shotNumber: Int,
)
