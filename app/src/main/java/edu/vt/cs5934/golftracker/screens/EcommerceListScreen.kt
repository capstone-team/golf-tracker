package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfContainer
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.viewmodels.EcommerceViewModel

@SuppressLint("SetJavaScriptEnabled")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EcommerceListScreen(
    navController: NavHostController,
    viewModel: EcommerceViewModel
) {
    val selectedClubType by viewModel.selectedClubType.collectAsState()
    val snackbarHostState = remember { SnackbarHostState() }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Golf Shop", color = Color.White) },
                colors = TopAppBarDefaults.topAppBarColors(containerColor = GolfGreen)
            )
        },
        bottomBar = {
            BottomNavBar(
                navController,
                "Home",
                "CommunityHub",
                "Events",
                R.string.home,
                R.string.community_hub,
                R.string.events,
                R.drawable.baseline_home_24,
                R.drawable.community_hub,
                R.drawable.events
            )
        },
        containerColor = GolfContainer,
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { paddingValues ->
        Column(modifier = Modifier.padding(paddingValues)) {
            ClubTypeFilter(
                clubTypes = viewModel.getClubTypes(),
                selectedType = selectedClubType,
                onTypeSelected = { viewModel.setSelectedClubType(it) }
            )

            AndroidView(
                factory = { context ->
                    WebView(context).apply {
                        webViewClient = WebViewClient()
                        settings.javaScriptEnabled = true
                    }
                },
                update = { webView ->
                    webView.loadUrl(viewModel.getClubUrl(selectedClubType))
                },
                modifier = Modifier.fillMaxSize()
            )
        }
    }
}

@Composable
fun ClubTypeFilter(
    clubTypes: List<String>,
    selectedType: String,
    onTypeSelected: (String) -> Unit
) {
    ScrollableTabRow(
        selectedTabIndex = clubTypes.indexOf(selectedType),
        modifier = Modifier.fillMaxWidth(),
        containerColor = Color.White,
        contentColor = GolfGreen
    ) {
        clubTypes.forEach { type ->
            Tab(
                selected = type == selectedType,
                onClick = { onTypeSelected(type) },
                text = { Text(type) }
            )
        }
    }
}