package edu.vt.cs5934.golftracker.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.viewmodels.SignUpViewModel

@Composable
fun SignUpScreen(
    navController: NavHostController,
    viewModel: SignUpViewModel
) {
    val uiState by viewModel.uiState
    val showEmailError = remember { mutableStateOf(false) }

    Scaffold { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .padding(0.dp, 100.dp, 0.dp, 0.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            ShotCaddieLogo()
            Spacer(modifier = Modifier.height(80.dp))
            Text(
                text = stringResource(R.string.sign_up_instruction),
            )
            Spacer(modifier = Modifier.height(16.dp))
            EmailField(uiState.email, viewModel::onEmailChange )
            Spacer(modifier = Modifier.height(16.dp))
            if(showEmailError.value) {
                EmailEntryError()
            }
            Button(
                modifier = Modifier
                    .width(330.dp)
                    .height(48.dp),
                shape = RoundedCornerShape(4.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Color.Black),
                onClick = {
                    if (!uiState.email.contains("@")) {
                        showEmailError.value = true
                    }
                    else {
                        navController.navigate("CreateAccount")
                    }
                },
            ) {
                Text(text = stringResource(R.string.sign_up_button),
                    style = MaterialTheme.typography.bodyLarge
                )
            }
            Spacer(modifier = Modifier.height(32.dp))
            Text(
                "or continue with",
                color = Color.Gray
            )
            Spacer(modifier = Modifier.height(32.dp))
            GoogleButton(navController)
        }
    }
}

@Composable
fun GoogleButton(navController: NavHostController) {
    Button(
        modifier = Modifier
            .width(330.dp),
        shape = RoundedCornerShape(4.dp),
        colors = ButtonDefaults.buttonColors(containerColor = Color.LightGray),
        onClick = { navController.navigate("Home") }
    ) {
        Image(
            painter = painterResource(R.drawable.ic_logo_google),
            contentDescription = "Google logo",
            modifier = Modifier
                .clip(CircleShape)
                .size(30.dp)
        )
        Text(text = stringResource(R.string.google),
            color = Color.Black,
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = FontWeight.Medium,
            modifier = Modifier.padding(6.dp))
    }
}

