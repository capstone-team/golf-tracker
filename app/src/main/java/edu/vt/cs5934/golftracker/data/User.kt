package edu.vt.cs5934.golftracker.data

import androidx.compose.runtime.mutableStateOf

data class User(
    var email: String,
    var guid: String,
    var handicap: Double = 0.0,
    var handicapDifferentials: MutableList<Double> = mutableListOf(),
    var listOfClubs: MutableMap<String, Pair<Int, Int>> = mutableMapOf(),
    var name: String = "Edit Name"
) {
    companion object {
        val currentUser = mutableStateOf(User("", ""))
    }
}