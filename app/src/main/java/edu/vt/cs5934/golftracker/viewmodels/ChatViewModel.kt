package edu.vt.cs5934.golftracker.viewmodels

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import edu.vt.cs5934.golftracker.data.ChatNotification
import edu.vt.cs5934.golftracker.data.Chats
import edu.vt.cs5934.golftracker.data.Message
import edu.vt.cs5934.golftracker.data.UserMetadata
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import java.util.UUID

class ChatViewModel(private val currentUserId: String, private val otherUserId: String) : ViewModel() {

    private val firestore = FirebaseFirestore.getInstance()
    private var chatListener: ListenerRegistration? = null

    private val _messages = MutableStateFlow<List<Message>>(emptyList())
    val messages: StateFlow<List<Message>> get() = _messages

    private val _notification = MutableStateFlow<ChatNotification?>(null)
    val notification: StateFlow<ChatNotification?> = _notification

    var messageText by mutableStateOf("")

    init {
        setupChatListener()
    }

    fun sendMessage() {
        if (messageText.isNotEmpty()) {
            viewModelScope.launch {
                sendMessageToFirestore(messageText)
                messageText = ""
            }
        }
    }

    fun onMessageTextChanged(newText: String) {
        messageText = newText
    }

    fun clearNotification() {
        _notification.value = null
    }

    private fun setupChatListener() {
        viewModelScope.launch {
            val chatId = getChatId(currentUserId, otherUserId)
            val chatRef = firestore.collection("chats").document(chatId)

            chatListener = chatRef.addSnapshotListener { snapshot, error ->
                if (error != null) {
                    Log.e("ChatViewModel", "Error listening to chat", error)
                    return@addSnapshotListener
                }
                if (snapshot != null && snapshot.exists()) {
                    val chat = snapshot.toObject(Chats::class.java)
                    chat?.let {
                        _messages.value = it.messages
                        if (it.messages.isNotEmpty()) {
                            val lastMessage = it.messages.last()
                            if (lastMessage.receiverId == currentUserId && lastMessage.senderId != currentUserId) {
                                createNotification(lastMessage)
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun sendMessageToFirestore(text: String) {
        try {
            val chatId = getChatId(currentUserId, otherUserId)
            val messageId = UUID.randomUUID().toString()
            val message = Message(
                id = messageId,
                senderId = currentUserId,
                receiverId = otherUserId,
                text = text,
                timestamp = Timestamp.now()
            )

            val chatRef = firestore.collection("chats").document(chatId)
            val chat = chatRef.get().await().toObject(Chats::class.java)

            if (chat == null) {
                createNewChat(chatRef, message)
            } else {
                updateExistingChat(chatRef, message)
            }

            updateActiveConversations(currentUserId, otherUserId)
            updateActiveConversations(otherUserId, currentUserId)
        } catch (e: Exception) {
            Log.e("ChatViewModel", "Error sending message", e)
        }
    }

    private suspend fun createNewChat(chatRef: DocumentReference, message: Message) {
        val newChat = Chats(
            chatId = chatRef.id,
            user1 = currentUserId,
            user2 = otherUserId,
            lastMessageTimestamp = message.timestamp,
            messages = listOf(message)
        )
        chatRef.set(newChat).await()
    }

    private suspend fun updateExistingChat(chatRef: DocumentReference, message: Message) {
        chatRef.update(
            "messages", FieldValue.arrayUnion(message),
            "lastMessageTimestamp", message.timestamp
        ).await()
    }

    private suspend fun updateActiveConversations(userId: String, otherUserId: String) {
        try {
            val userMetadataRef = firestore.collection("userMetadata").document(userId)
            val document = userMetadataRef.get().await()

            if (document.exists()) {
                userMetadataRef.update("activeConversations", FieldValue.arrayUnion(otherUserId)).await()
                Log.d("ChatViewModel", "Active conversation updated for user $userId")
            } else {
                val newMetadata = UserMetadata(userId = userId, activeConversations = listOf(otherUserId))
                userMetadataRef.set(newMetadata).await()
                Log.d("ChatViewModel", "User metadata created and active conversation added for user $userId")
            }
        } catch (e: Exception) {
            Log.e("ChatViewModel", "Error updating active conversation for user $userId", e)
        }
    }

    private suspend fun getChatId(userId1: String, userId2: String): String {
        val chatId1 = "$userId1$userId2"
        val chatId2 = "$userId2$userId1"

        val chat1Exists = firestore.collection("chats").document(chatId1).get().await().exists()
        return if (chat1Exists) chatId1 else chatId2
    }

    private suspend fun getUserName(userId: String): String {
        return try {
            val userDoc = firestore.collection("users").document(userId).get().await()
            userDoc.getString("name") ?: "Unknown User"
        } catch (e: Exception) {
            Log.e("ChatViewModel", "Error fetching user name", e)
            "Unknown User"
        }
    }

    private fun createNotification(message: Message) {
        viewModelScope.launch {
            val chatId = getChatId(currentUserId, otherUserId)
            val senderName = getUserName(message.senderId)
            _notification.value = ChatNotification(
                title = "New Message from $senderName",
                body = message.text,
                chatId = chatId,
                messageId = message.id,
                senderId = message.senderId,
                senderName = senderName
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        chatListener?.remove()
    }

}

class ChatViewModelFactory(private val currentUserId: String, private val otherUserId: String) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ChatViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ChatViewModel(currentUserId, otherUserId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}