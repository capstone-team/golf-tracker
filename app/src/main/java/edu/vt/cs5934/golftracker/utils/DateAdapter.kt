package edu.vt.cs5934.golftracker.utils

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import edu.vt.cs5934.golftracker.data.DateRange


class DateAdapter {
    @FromJson
    fun fromJson(json: Map<*, *>): DateRange {
        return DateRange(
            weekNumber = json["weekNumber"]?: "",
            start = (json["start"]) ?: "",
            end = (json["end"]) ?: ""
        )
    }

    @ToJson
    fun toJson(dateRange: DateRange): Map<String, Any?> {
        return mapOf(
            "weekNumber" to dateRange.weekNumber,
            "start" to dateRange.start,
            "end" to dateRange.end
        )
    }
}
