package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import android.graphics.Paint
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.navigation.TopNavStatistics
import edu.vt.cs5934.golftracker.ui.theme.GolfBlack
import edu.vt.cs5934.golftracker.ui.theme.GolfGreen
import edu.vt.cs5934.golftracker.ui.theme.GolfRed
import edu.vt.cs5934.golftracker.ui.theme.PieChartBlue
import edu.vt.cs5934.golftracker.ui.theme.PieChartGreen
import edu.vt.cs5934.golftracker.viewmodels.StatisticsViewModel
import java.util.Locale
/*
Attribution given to Georgios Patronas for part of the code for the DonutChart and PieChart.  His
Github repository is at https://github.com/giorgospat/compose-charts
 */

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun StatisticsScreen(
    navController: NavHostController,
    statisticsViewModel: StatisticsViewModel
) {
    Scaffold(
        topBar = { TopNavStatistics(
            courseName = statisticsViewModel.selectedRound.value.courseName,
            date = statisticsViewModel.selectedRound.value.date
        ) },
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "Scorecard" ,
            "Course",
            R.string.home,
            R.string.scorecard,
            R.string.course_map,
            R.drawable.baseline_home_24,
            R.drawable.ic_score_icon,
            R.drawable.baseline_map_24
        ) }
    ){
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = GolfBlack),
            verticalArrangement = Arrangement.Center
        ) {
            TargetLegend(statisticsViewModel)
            Row(
                modifier = Modifier
                    .padding(10.dp, 0.dp, 10.dp, 10.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround,
            ) {
                StatisticsBox(
                    title = stringResource(R.string.putts_per_hole),
                    value = statisticsViewModel
                        .getAvgNumOfPutts(statisticsViewModel.selectedRound.value).toString()
                )
                Column(
                    modifier = Modifier
                        .height(180.dp)
                        .width(180.dp)
                        .background(color = Color.White, shape = RoundedCornerShape(2.dp)),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        modifier = Modifier
                            .padding(0.dp, 10.dp, 0.dp, 3.dp),
                        text = stringResource(R.string.targets),
                        style = MaterialTheme.typography.titleMedium,
                        fontWeight = FontWeight.Bold,
                    )
                    PieChart(statisticsViewModel.targets)
                }
            }
            Row(
                modifier = Modifier
                    .padding(10.dp, 0.dp, 10.dp, 10.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                Column(
                    modifier = Modifier
                        .height(180.dp)
                        .width(180.dp)
                        .background(color = Color.White, shape = RoundedCornerShape(2.dp)),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        modifier = Modifier
                            .padding(0.dp, 10.dp, 0.dp, 7.dp),
                        text = stringResource(R.string.gir),
                        style = MaterialTheme.typography.titleMedium,
                        fontWeight = FontWeight.Bold,
                    )
                    DonutChart(statisticsViewModel.greensInReg)
                }
                StatisticsBox(
                    title = stringResource(R.string.three_putts),
                    value = statisticsViewModel
                        .getNumOfThreePutts(statisticsViewModel.selectedRound.value).toString()
                )
            }
            Row(
                modifier = Modifier
                    .padding(10.dp, 0.dp, 10.dp, 0.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                StatisticsBox(
                    title = stringResource(R.string.driving_distance),
                    value = "${statisticsViewModel
                        .getAvgDrivingDistance(statisticsViewModel.selectedRound.value)}y"
                )
                StatisticsBox(
                    title = stringResource(R.string.longest_drive),
                    value = "${statisticsViewModel
                        .getLongestDrive(statisticsViewModel.selectedRound.value)}y"
                )
            }
        }
    }
}

@Composable
fun StatisticsBox(
    title: String,
    value: String
) {
    Column(
        modifier = Modifier
            .height(180.dp)
            .width(180.dp)
            .background(color = Color.White, shape = RoundedCornerShape(2.dp)),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            modifier = Modifier
                .padding(0.dp, 10.dp, 0.dp, 0.dp),
            text = title,
            style = MaterialTheme.typography.titleMedium,
            fontWeight = FontWeight.Bold,
        )
        Text(
            modifier = Modifier
                .padding(0.dp, 40.dp, 0.dp, 0.dp),
            text = value,
            style = MaterialTheme.typography.titleMedium,
            fontWeight = FontWeight.Medium,
            fontSize = 40.sp,
            color = if (title == "3 Putts+") GolfRed else GolfGreen
        )
    }
}

@Composable
fun PieChart(inputValues: List<Float>)  {
    val chartDegrees = 360f
    var startAngle = 270f
    val colors = listOf(PieChartGreen, PieChartBlue, GolfRed)
    val proportions = inputValues.map {
        it * 100 / inputValues.sum()
    }

    val angleProgress = proportions.map { prop ->
        chartDegrees * prop / 100
    }
    Box(
        modifier = Modifier,
        contentAlignment = Alignment.Center
    ) {
        val canvasSize = 360 //min(constraints.maxWidth, constraints.maxHeight)
        val size = Size(canvasSize.toFloat(), canvasSize.toFloat())
        val canvasSizeDp = with(LocalDensity.current) { canvasSize.toDp() }

        Canvas(modifier = Modifier.size(canvasSizeDp)) {
            angleProgress.forEachIndexed { index, angle ->
                drawArc(
                    color = colors[index],
                    startAngle = startAngle,
                    sweepAngle = angle,
                    useCenter = true,
                    size = size,
                    style = Fill
                )
                startAngle += angle
            }
        }
    }
}

@Composable
fun DonutChart(inputValues: List<Float>)  {
    val chartDegrees = 360f
    var startAngle = 270f
    val colors = listOf(PieChartGreen, Color.LightGray)
    val proportions = inputValues.map {
        it * 100 / inputValues.sum()
    }

    val angleProgress = proportions.map { prop ->
        chartDegrees * prop / 100
    }

    // text style
    val density = LocalDensity.current
    val textFontSize = with(density) { 30.dp.toPx() }
    val textPaint = remember {
        Paint().apply {
            color = GolfBlack.toArgb()
            textSize = textFontSize
            textAlign = Paint.Align.CENTER
        }
    }
    Box(
        modifier = Modifier,
        contentAlignment = Alignment.Center
    ) {
        val canvasSize = 330
        val size = Size(canvasSize.toFloat(), canvasSize.toFloat())
        val canvasSizeDp = with(LocalDensity.current) { canvasSize.toDp() }
        val sliceWidth = with(LocalDensity.current) { 15.dp.toPx() }

        Canvas(modifier = Modifier.size(canvasSizeDp)) {
            angleProgress.forEachIndexed { index, angle ->
                drawArc(
                    color = colors[index],
                    startAngle = startAngle,
                    sweepAngle = angle,
                    useCenter = false,
                    size = size,
                    style = Stroke(width = sliceWidth)
                )
                startAngle += angle
            }
            drawIntoCanvas { canvas ->
                val text = String.format(Locale.US, "%.1f", proportions[0].toDouble())
                canvas.nativeCanvas.drawText(
                    "$text%",
                    (canvasSize / 2) + textFontSize / 4,
                    (canvasSize / 2) + textFontSize / 4,
                    textPaint
                )
            }
        }
    }
}

@Composable
fun TargetLegend(statisticsViewModel: StatisticsViewModel) {
    val score = statisticsViewModel.selectedRound.value.roundScore
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(17.dp, 0.dp, 13.dp, 5.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = if (score > 0) "Score: +${score}"
                    else if (score < 0) "Score: -${score}"
                    else "Score: 0",
            color = Color.White
        )
        Row {
            Row(modifier = Modifier
                .padding(0.dp, 0.dp, 5.dp, 0.dp)
            ) {
                Text(
                    text = "Birdies: ",
                    color = Color.White
                )
                Row(modifier = Modifier
                    .height(22.dp)
                    .width(22.dp)
                    .background(color = PieChartGreen),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(statisticsViewModel.targets[0].toInt().toString())
                }
            }
            Row(modifier = Modifier
                .padding(0.dp, 0.dp, 5.dp, 0.dp)
            ) {
                Text(
                    text = "Pars: ",
                    color = Color.White
                )
                Row(modifier = Modifier
                    .height(22.dp)
                    .width(22.dp)
                    .background(color = PieChartBlue),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(statisticsViewModel.targets[1].toInt().toString())
                }
            }
            Row(modifier = Modifier
                .padding(0.dp, 0.dp, 5.dp, 0.dp)
            ) {
                Text(
                    text = "Bogeys: ",
                    color = Color.White
                )
                Row(modifier = Modifier
                    .height(22.dp)
                    .width(22.dp)
                    .background(color = GolfRed),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(statisticsViewModel.targets[2].toInt().toString())
                }
            }
        }
    }
}