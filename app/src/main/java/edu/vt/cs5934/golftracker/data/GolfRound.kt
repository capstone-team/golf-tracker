package edu.vt.cs5934.golftracker.data

import androidx.compose.runtime.mutableStateOf

data class GolfRound(
    var clubsUsed: MutableMap<String, Pair<Int, Int>> = mutableMapOf(),
    var courseName: String = "",
    var date: String = "",
    var listOfGolfHoles: MutableList<GolfHole> = mutableListOf(),
    var listOfHoleScores: MutableList<Int> = mutableListOf(),
    var roundScore: Int = 0,
) {
    companion object {
        val golfRound = mutableStateOf(GolfRound())
    }
}