package edu.vt.cs5934.golftracker.navigation

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.ui.theme.GolfBlue

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopNavCourse(courseName: String) {
    CenterAlignedTopAppBar(
        title = { Text(text = courseName) },
        colors = TopAppBarColors(
            containerColor = Color.White.copy(alpha = .9f),
            scrolledContainerColor = Color.Transparent,
            navigationIconContentColor = Color.Transparent,
            titleContentColor = Color(0xFF262522),
            actionIconContentColor = Color.Transparent
        )
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopNavStatistics(courseName: String, date: String) {
    CenterAlignedTopAppBar(
        title = {
            Text(
                buildAnnotatedString {
                    append("$courseName\n")
                    withStyle(style = SpanStyle(
                        fontSize = 18.sp,
                        color = GolfBlue)
                    ) {
                        append(date)
                    }
                },
                textAlign = TextAlign.Center
            )
        },
        colors = TopAppBarColors(
            containerColor = Color.White.copy(alpha = .9f),
            scrolledContainerColor = Color.Transparent,
            navigationIconContentColor = Color.Transparent,
            titleContentColor = Color(0xFF262522),
            actionIconContentColor = Color.Transparent
        )
    )
}

@Composable
fun BottomNavBar(
    navController: NavHostController,
    stringOne: String,
    stringTwo: String,
    stringThree: String,
    sROne: Int,
    sRTwo: Int,
    sRThree: Int,
    pROne: Int,
    pRTwo: Int,
    pRThree: Int
) {
    NavigationBar(modifier = Modifier
        .padding(0.dp, 4.dp, 0.dp, 4.dp),
        containerColor = Color.White
    ) {
        NavigationBarItem(
            selected = false,
            onClick = { navController.navigate(stringOne) },
            label = {
                Text(text = stringResource(sROne))
            },
            icon = {
                Icon(
                    painter = painterResource(pROne),
                    contentDescription = stringResource(sROne)
                )
            }
        )
        NavigationBarItem(
            selected = false,
            onClick = { navController.navigate(stringTwo)},
            label = {
                Text(text = stringResource(sRTwo))
            },
            icon = {
                Icon(
                    painter = painterResource(pRTwo),
                    contentDescription = stringResource(sRTwo),
                    Modifier.size(24.dp)
                )
            }
        )
        NavigationBarItem(
            selected = false,
            onClick = { navController.navigate(stringThree) },
            label = {
                Text(text = stringResource(sRThree))
            },
            icon = {
                Icon(
                    painter = painterResource(pRThree),
                    contentDescription = stringResource(sRThree)
                )
            }
        )
    }
}