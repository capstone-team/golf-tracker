package edu.vt.cs5934.golftracker.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import edu.vt.cs5934.golftracker.viewmodels.ProfileViewModel
import edu.vt.cs5934.golftracker.R
import edu.vt.cs5934.golftracker.data.User
import edu.vt.cs5934.golftracker.navigation.BottomNavBar
import edu.vt.cs5934.golftracker.ui.theme.GolfBlack
import edu.vt.cs5934.golftracker.ui.theme.GolfRed
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun ProfileScreen(
    navController: NavHostController,
    profileViewModel: ProfileViewModel,
    ) {
    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val editMode = remember { mutableStateOf(false) }
    val name = remember { mutableStateOf(User.currentUser.value.name) }
    val handicap = remember { mutableStateOf(User.currentUser.value.handicap.toString()) }
    val homeCourse = remember { mutableStateOf("Reston Golf Course") }
    val email = remember { mutableStateOf(User.currentUser.value.email) }

    Scaffold(
        bottomBar = { BottomNavBar(
            navController,
            "Home",
            "CommunityHub" ,
            "Events",
            R.string.home,
            R.string.community_hub,
            R.string.events,
            R.drawable.baseline_home_24,
            R.drawable.community_hub,
            R.drawable.events
        ) },
        containerColor = Color(0xFFF9FFF8),
        snackbarHost = { SnackbarHost(hostState = snackbarHostState) }
    ) { _ ->
        Column(
            modifier = Modifier
                .padding(16.dp, 0.dp, 16.dp, 75.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                text = stringResource(R.string.player_card),
                style = MaterialTheme.typography.headlineLarge
            )
            Image(
                painter = painterResource(R.drawable.ic_card_icon),
                contentDescription = "image of golf ball",
                modifier = Modifier
                    .clip(CircleShape)
                    .size(180.dp)
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Button(
                    modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 0.dp),
                    shape = RoundedCornerShape(4.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = GolfBlack),
                    onClick = { editMode.value = !editMode.value
                        profileViewModel.editOnClick(editMode.value, name.value, handicap.value, email.value)
                    }) {
                    Text(modifier = Modifier.padding(0.dp, 0.dp, 10.dp, 0.dp),
                        text = if (!editMode.value)
                            stringResource(R.string.edit_profile) else stringResource(R.string.save))
                    Icon(
                        imageVector = if (!editMode.value) Icons.Default.Edit else Icons.Default.CheckCircle,
                        contentDescription = "edit or save icon"
                    )
                }
                OutlinedTextField(modifier = Modifier
                    .width(170.dp),
                    value = name.value,
                    onValueChange = { name.value = it },
                    readOnly = !editMode.value,
                    textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End),
                    colors = OutlinedTextFieldDefaults.colors(
                        focusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f),
                        unfocusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f)
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Text(
                    text = stringResource(R.string.handicap),
                    fontWeight = FontWeight.Bold
                )
                OutlinedTextField(modifier = Modifier
                    .width(250.dp),
                    value = handicap.value,
                    onValueChange = { handicap.value = it },
                    readOnly = true,
                    textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End),
                    colors = OutlinedTextFieldDefaults.colors(
                        focusedBorderColor = Color.Black.copy(alpha = 0f),
                        unfocusedBorderColor = Color.Black.copy(alpha = 0f)
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Text(
                    text = stringResource(R.string.home_course),
                    fontWeight = FontWeight.Bold
                )
                OutlinedTextField(
                    modifier = Modifier
                        .width(250.dp),
                    value = homeCourse.value,
                    onValueChange = { homeCourse.value = it },
                    readOnly = !editMode.value,
                    textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End),
                    colors = OutlinedTextFieldDefaults.colors(
                        focusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f),
                        unfocusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f)
                    )
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Text(
                    text = stringResource(R.string.email),
                    fontWeight = FontWeight.Bold
                )
                OutlinedTextField(
                    modifier = Modifier
                        .width(250.dp),
                    value = email.value,
                    onValueChange = { email.value = it},
                    readOnly = !editMode.value,
                    textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.End),
                    colors = OutlinedTextFieldDefaults.colors(
                        focusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f),
                        unfocusedBorderColor = if (!editMode.value) Color.Black.copy(alpha = 0f)
                            else Color.Black.copy(alpha = .1f)
                    )
                )
            }
            Column {
                Button(
                    modifier = Modifier
                        .width(330.dp)
                        .height(48.dp)
                        .align(Alignment.CenterHorizontally),
                    shape = RoundedCornerShape(4.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = GolfBlack),
                    onClick = {
                        profileViewModel.onSignOut()
                        navController.navigate("SignIn") {
                            popUpTo("Home") { inclusive = true }
                        }
                    },
                ) {
                    Text(modifier = Modifier,
                        text = stringResource(R.string.sign_out),
                        style = MaterialTheme.typography.bodyLarge
                    )
                }
                Button(
                    modifier = Modifier
                        .width(330.dp)
                        .height(48.dp)
                        .align(Alignment.CenterHorizontally),
                    shape = RoundedCornerShape(4.dp),
                    colors = ButtonDefaults.buttonColors(containerColor = GolfRed),
                    onClick = {
                        profileViewModel.deleteAccount(scope, snackbarHostState) {
                            navController.navigate("SignIn") {
                                popUpTo("Home") { inclusive = true }
                            }
                        }
                    },
                ) {
                    Text(
                        text = stringResource(R.string.delete_account),
                        style = MaterialTheme.typography.bodyLarge
                    )
                }
            }
        }
        LaunchedEffect(User.currentUser.value.name) {
            if (User.currentUser.value.name == "Edit Name") {
                scope.launch {
                    snackbarHostState.showSnackbar("Please edit your name before continuing")
                }
            }
        }
    }
}